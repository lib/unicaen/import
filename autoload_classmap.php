<?php
// Generated by UnicaenCode
return [
    'SourceFormHydrator'                                                                     => __DIR__ . '/src/Form/SourceForm.php',
    'TableFormHydrator'                                                                      => __DIR__ . '/src/Form/TableForm.php',
    'UnicaenImportTest\Bootstrap'                                                            => __DIR__ . '/tests/Bootstrap.php',
    'UnicaenImport\Controller\DifferentielController'                                        => __DIR__ . '/src/Controller/DifferentielController.php',
    'UnicaenImport\Controller\Factory\DifferentielControllerFactory'                         => __DIR__ . '/src/Controller/Factory/DifferentielControllerFactory.php',
    'UnicaenImport\Controller\Factory\ImportControllerFactory'                               => __DIR__ . '/src/Controller/Factory/ImportControllerFactory.php',
    'UnicaenImport\Controller\Factory\SourceControllerFactory'                               => __DIR__ . '/src/Controller/Factory/SourceControllerFactory.php',
    'UnicaenImport\Controller\Factory\TableControllerFactory'                                => __DIR__ . '/src/Controller/Factory/TableControllerFactory.php',
    'UnicaenImport\Controller\ImportController'                                              => __DIR__ . '/src/Controller/ImportController.php',
    'UnicaenImport\Controller\SourceController'                                              => __DIR__ . '/src/Controller/SourceController.php',
    'UnicaenImport\Controller\TableController'                                               => __DIR__ . '/src/Controller/TableController.php',
    'UnicaenImport\Entity\Db\Interfaces\ImportAwareInterface'                                => __DIR__ . '/src/Entity/Db/Interfaces/ImportAwareInterface.php',
    'UnicaenImport\Entity\Db\Interfaces\SourceAwareInterface'                                => __DIR__ . '/src/Entity/Db/Interfaces/SourceAwareInterface.php',
    'UnicaenImport\Entity\Db\Source'                                                         => __DIR__ . '/src/Entity/Db/Source.php',
    'UnicaenImport\Entity\Db\Table'                                                          => __DIR__ . '/src/Entity/Db/Table.php',
    'UnicaenImport\Entity\Db\Traits\ImportAwareTrait'                                        => __DIR__ . '/src/Entity/Db/Traits/ImportAwareTrait.php',
    'UnicaenImport\Entity\Db\Traits\SourceAwareTrait'                                        => __DIR__ . '/src/Entity/Db/Traits/SourceAwareTrait.php',
    'UnicaenImport\Entity\Differentiel\Ligne'                                                => __DIR__ . '/src/Entity/Differentiel/Ligne.php',
    'UnicaenImport\Entity\Differentiel\Query'                                                => __DIR__ . '/src/Entity/Differentiel/Query.php',
    'UnicaenImport\Entity\Schema\Column'                                                     => __DIR__ . '/src/Entity/Schema/Column.php',
    'UnicaenImport\Exception\Exception'                                                      => __DIR__ . '/src/Exception/Exception.php',
    'UnicaenImport\Exception\MissingDependency'                                              => __DIR__ . '/src/Exception/MissingDependency.php',
    'UnicaenImport\Form\Factory\SourceFormFactory'                                           => __DIR__ . '/src/Form/Factory/SourceFormFactory.php',
    'UnicaenImport\Form\Factory\TableFormFactory'                                            => __DIR__ . '/src/Form/Factory/TableFormFactory.php',
    'UnicaenImport\Form\SourceForm'                                                          => __DIR__ . '/src/Form/SourceForm.php',
    'UnicaenImport\Form\TableForm'                                                           => __DIR__ . '/src/Form/TableForm.php',
    'UnicaenImport\Form\Traits\SourceFormAwareTrait'                                         => __DIR__ . '/src/Form/Traits/SourceFormAwareTrait.php',
    'UnicaenImport\Form\Traits\TableFormAwareTrait'                                          => __DIR__ . '/src/Form/Traits/TableFormAwareTrait.php',
    'UnicaenImport\Module'                                                                   => __DIR__ . '/Module.php',
    'UnicaenImport\ORM\Event\Listeners\EntitySourceInjector'                                 => __DIR__ . '/src/ORM/Event/Listeners/EntitySourceInjector.php',
    'UnicaenImport\ORM\Event\Listeners\Factory\EntitySourceInjectorFactory'                  => __DIR__ . '/src/ORM/Event/Listeners/Factory/EntitySourceInjectorFactory.php',
    'UnicaenImport\ORM\Query\Functions\PasHistorise'                                         => __DIR__ . '/src/ORM/Query/Functions/PasHistorise.php',
    'UnicaenImport\Options\Factory\ModuleOptionsFactory'                                     => __DIR__ . '/src/Options/Factory/ModuleOptionsFactory.php',
    'UnicaenImport\Options\ModuleOptions'                                                    => __DIR__ . '/src/Options/ModuleOptions.php',
    'UnicaenImport\Options\Traits\ModuleOptionsAwareTrait'                                   => __DIR__ . '/src/Options/Traits/ModuleOptionsAwareTrait.php',
    'UnicaenImport\Processus\Factory\ImportProcessusFactory'                                 => __DIR__ . '/src/Processus/Factory/ImportProcessusFactory.php',
    'UnicaenImport\Processus\ImportProcessus'                                                => __DIR__ . '/src/Processus/ImportProcessus.php',
    'UnicaenImport\Processus\Traits\ImportProcessusAwareTrait'                               => __DIR__ . '/src/Processus/Traits/ImportProcessusAwareTrait.php',
    'UnicaenImport\Provider\Privilege\Privileges'                                            => __DIR__ . '/src/Provider/Privilege/Privileges.php',
    'UnicaenImport\Service\AbstractService'                                                  => __DIR__ . '/src/Service/AbstractService.php',
    'UnicaenImport\Service\DifferentielService'                                              => __DIR__ . '/src/Service/DifferentielService.php',
    'UnicaenImport\Service\Factory\DifferentielServiceFactory'                               => __DIR__ . '/src/Service/Factory/DifferentielServiceFactory.php',
    'UnicaenImport\Service\Factory\QueryGeneratorServiceFactory'                             => __DIR__ . '/src/Service/Factory/QueryGeneratorServiceFactory.php',
    'UnicaenImport\Service\Factory\SchemaServiceFactory'                                     => __DIR__ . '/src/Service/Factory/SchemaServiceFactory.php',
    'UnicaenImport\Service\Factory\SourceServiceFactory'                                     => __DIR__ . '/src/Service/Factory/SourceServiceFactory.php',
    'UnicaenImport\Service\Factory\TableServiceFactory'                                      => __DIR__ . '/src/Service/Factory/TableServiceFactory.php',
    'UnicaenImport\Service\QueryGeneratorService'                                            => __DIR__ . '/src/Service/QueryGeneratorService.php',
    'UnicaenImport\Service\SchemaService'                                                    => __DIR__ . '/src/Service/SchemaService.php',
    'UnicaenImport\Service\SourceService'                                                    => __DIR__ . '/src/Service/SourceService.php',
    'UnicaenImport\Service\TableService'                                                     => __DIR__ . '/src/Service/TableService.php',
    'UnicaenImport\Service\Traits\DifferentielServiceAwareTrait'                             => __DIR__ . '/src/Service/Traits/DifferentielServiceAwareTrait.php',
    'UnicaenImport\Service\Traits\QueryGeneratorServiceAwareTrait'                           => __DIR__ . '/src/Service/Traits/QueryGeneratorServiceAwareTrait.php',
    'UnicaenImport\Service\Traits\SchemaServiceAwareTrait'                                   => __DIR__ . '/src/Service/Traits/SchemaServiceAwareTrait.php',
    'UnicaenImport\Service\Traits\SourceServiceAwareTrait'                                   => __DIR__ . '/src/Service/Traits/SourceServiceAwareTrait.php',
    'UnicaenImport\Service\Traits\TableServiceAwareTrait'                                    => __DIR__ . '/src/Service/Traits/TableServiceAwareTrait.php',
    'UnicaenImport\View\Helper\DifferentielLigne\DifferentielLigne'                          => __DIR__ . '/src/View/Helper/DifferentielLigne/DifferentielLigne.php',
    'UnicaenImport\View\Helper\DifferentielLigne\Factory\DifferentielLigneViewHelperFactory' => __DIR__ . '/src/View/Helper/DifferentielLigne/Factory/DifferentielLigneViewHelperFactory.php',
    'UnicaenImport\View\Helper\DifferentielListe'                                            => __DIR__ . '/src/View/Helper/DifferentielListe.php',
    'UnicaenImport\View\Helper\Factory\DifferentielListeViewHelperFactory'                   => __DIR__ . '/src/View/Helper/Factory/DifferentielListeViewHelperFactory.php',
];