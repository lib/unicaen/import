<?php

namespace UnicaenImport;

use UnicaenPrivilege\Guard\PrivilegeController;
use UnicaenImport\Provider\Privilege\Privileges;

return [

    'router' => [
        'routes' => [
            'import' => [
                'type'          => 'Literal',
                'options'       => [
                    'route'    => '/import',
                    'defaults' => [
                        '__NAMESPACE__' => 'Import\Controller',
                        'controller'    => 'Import',
                        'action'        => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'tableau-bord'       => [
                        'type'    => 'Literal',
                        'options' => [
                            'route'    => '/tableau-bord',
                            'defaults' => [
                                'action' => 'tableau-bord',
                            ],
                        ],
                    ],
                    'maj-vues-fonctions' => [
                        'type'    => 'Literal',
                        'options' => [
                            'route'    => '/maj-vues-fonctions',
                            'defaults' => [
                                'action' => 'maj-vues-fonctions',
                            ],
                        ],
                    ],
                    'sources'            => [
                        'type'          => 'Literal',
                        'options'       => [
                            'route'    => '/sources',
                            'defaults' => [
                                'controller' => 'Source',
                                'action'     => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'edition'     => [
                                'type'          => 'Segment',
                                'options'       => [
                                    'route'       => '/edition[/:source]',
                                    'constraints' => [
                                        'source' => '[0-9]*',
                                    ],
                                    'defaults'    => [
                                        'action' => 'edition',
                                    ],
                                ],
                                'may_terminate' => true,
                            ],
                            'suppression' => [
                                'type'          => 'Segment',
                                'options'       => [
                                    'route'       => '/suppression/:source',
                                    'constraints' => [
                                        'source' => '[0-9]*',
                                    ],
                                    'defaults'    => [
                                        'action' => 'suppression',
                                    ],
                                ],
                                'may_terminate' => true,
                            ],
                        ],
                    ],
                    'differentiel'       => [
                        'type'          => 'Literal',
                        'options'       => [
                            'route'    => '/differentiel',
                            'defaults' => [
                                'controller' => 'Differentiel',
                                'action'     => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'maj-vue-materialisee' => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/maj-vue-materialisee[/:table]',
                                    'defaults' => [
                                        'action' => 'maj-vue-materialisee',
                                    ],
                                ],
                            ],
                            'synchronisation'      => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/synchronisation[/:table]',
                                    'defaults' => [
                                        'action' => 'synchronisation',
                                    ],
                                ],
                            ],
                            'details'              => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/details[/:table]',
                                    'defaults' => [
                                        'action' => 'details',
                                    ],
                                ],
                            ],

                        ],
                    ],
                    'tables'             => [
                        'type'          => 'Literal',
                        'options'       => [
                            'route'    => '/tables',
                            'defaults' => [
                                'controller' => 'Table',
                                'action'     => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'add-non-referenced' => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/add-non-referenced',
                                    'defaults' => [
                                        'action' => 'add-non-referenced',
                                    ],
                                ],
                            ],
                            'synchro-switch'     => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/synchro-switch',
                                    'defaults' => [
                                        'action' => 'synchro-switch',
                                    ],
                                ],
                            ],
                            'trier'              => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/trier',
                                    'defaults' => [
                                        'action' => 'trier',
                                    ],
                                ],
                            ],
                            'modifier'           => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/modifier/:table',
                                    'defaults' => [
                                        'action' => 'modifier',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],


    'laminas-cli' => [
        'commands' => [
            'unicaen-import:sync-job'           => Command\SyncJobCommand::class,
            'unicaen-import:maj-vues-fonctions' => Command\MajVuesFonctionsCommand::class,
        ],
    ],


    'navigation' => [
        'default' => [
            'home' => [
                'pages' => [
                    'import' => [
                        'label'    => 'Synchronisation',
                        'order'    => 1,
                        'route'    => 'import',
                        'resource' => PrivilegeController::getResourceId('Import\Controller\Import', 'index'),
                        'pages'    => [
                            'sources'            => [
                                'label'       => "Sources de données",
                                'description' => "Liste des sources de données",
                                'route'       => 'import/sources',
                                'resource'    => PrivilegeController::getResourceId('Import\Controller\Source', 'index'),
                            ],
                            'tables'             => [
                                'label'    => 'Tables',
                                'route'    => 'import/tables',
                                'resource' => PrivilegeController::getResourceId('Import\Controller\Table', 'index'),
                            ],
                            'tableau-bord'       => [
                                'label'       => "Tableau de bord principal",
                                'description' => "Liste, table par table, les colonnes dont les données sont importables ou non, leur caractéristiques et l'état de l'import à leur niveau.",
                                'route'       => 'import/tableau-bord',
                                'resource'    => PrivilegeController::getResourceId('Import\Controller\Import', 'tableau-bord'),
                            ],
                            'differentiel'       => [
                                'label'       => "Écarts entre les données de l'application et ses sources",
                                'description' => "Affiche, table par table, la liste des données différentes entre l'application et ses sources de données",
                                'route'       => 'import/differentiel',
                                'resource'    => PrivilegeController::getResourceId('Import\Controller\Differentiel', 'index'),
                            ],
                            'maj-vues-fonctions' => [
                                'label'       => "Mise à jour des vues différentielles et des procédures de mise à jour",
                                'description' => "Réactualise les vues différentielles d'import. Ces dernières servent à déterminer quelles données ont changé,
        sont apparues ou ont disparues des sources de données.
        Met également à jour les procédures de mise à jour qui actualisent les données de l'application à partir des informations
        fournies par les vues différentielles.
        Cette réactualisation n'est utile que si les vues sources ont été modifiées.",
                                'route'       => 'import/maj-vues-fonctions',
                                'resource'    => PrivilegeController::getResourceId('Import\Controller\Import', 'update-views-and-packages'),
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                /* Import */
                [
                    'controller' => 'Import\Controller\Import',
                    'action'     => ['index'],
                    'privileges' => [
                        Privileges::IMPORT_ECARTS,
                        Privileges::IMPORT_MAJ,
                        Privileges::IMPORT_TBL,
                        Privileges::IMPORT_VUES_PROCEDURES,
                        Privileges::IMPORT_SOURCES_VISUALISATION,
                        Privileges::IMPORT_TABLES_VISUALISATION,
                    ],
                ],
                [
                    'controller' => 'Import\Controller\Import',
                    'action'     => ['tableau-bord'],
                    'privileges' => [Privileges::IMPORT_TBL],
                ],
                [
                    'controller' => 'Import\Controller\Import',
                    'action'     => ['maj-vues-fonctions'],
                    'privileges' => [Privileges::IMPORT_VUES_PROCEDURES],
                ],

                /* Gestion des sources */
                [
                    'controller' => 'Import\Controller\Source',
                    'action'     => ['index'],
                    'privileges' => [Privileges::IMPORT_SOURCES_VISUALISATION],
                ],
                [
                    'controller' => 'Import\Controller\Source',
                    'action'     => ['edition', 'suppression'],
                    'privileges' => [Privileges::IMPORT_SOURCES_EDITION],
                ],

                /* Tables */
                [
                    'controller' => 'Import\Controller\Table',
                    'action'     => ['index'],
                    'privileges' => [Privileges::IMPORT_TABLES_VISUALISATION],
                ],
                [
                    'controller' => 'Import\Controller\Table',
                    'action'     => ['add-non-referenced', 'synchro-switch', 'trier', 'modifier'],
                    'privileges' => [Privileges::IMPORT_TABLES_EDITION],
                ],

                /* Différentiel */
                [
                    'controller' => 'Import\Controller\Differentiel',
                    'action'     => ['index', 'details'],
                    'privileges' => [
                        Privileges::IMPORT_ECARTS,
                    ],
                ],
                [
                    'controller' => 'Import\Controller\Differentiel',
                    'action'     => ['synchronisation'],
                    'privileges' => [
                        Privileges::IMPORT_ECARTS,
                        Privileges::IMPORT_MAJ,
                    ],
                ],
            ],
        ],
    ],

    'doctrine' => [
        'driver'        => [
            'unicaen_import_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity/Db',
                ],
            ],
            'orm_default'           => [
                'class'   => 'Doctrine\ORM\Mapping\Driver\DriverChain',
                'drivers' => [
                    'UnicaenImport\Entity\Db' => 'unicaen_import_driver',
                ],
            ],
        ],
        'configuration' => [
            'orm_default' => [
                'string_functions' => [
                    'pasHistorise' => ORM\Query\Functions\PasHistorise::class,
                ],
            ],
        ],
        'eventmanager'  => [
            'orm_default' => [
                'subscribers' => [
                    ORM\Event\Listeners\EntitySourceInjector::class,
                ],
            ],
        ],
    ],

    'controllers' => [
        'factories' => [
            'Import\Controller\Import'       => Controller\Factory\ImportControllerFactory::class,
            'Import\Controller\Source'       => Controller\Factory\SourceControllerFactory::class,
            'Import\Controller\Table'        => Controller\Factory\TableControllerFactory::class,
            'Import\Controller\Differentiel' => Controller\Factory\DifferentielControllerFactory::class,
        ],
    ],

    'service_manager' => [
        'factories' => [
            Options\ModuleOptions::class                    => Options\Factory\ModuleOptionsFactory::class,
            Service\SchemaService::class                    => Service\Factory\SchemaServiceFactory::class,
            Service\QueryGeneratorService::class            => Service\Factory\QueryGeneratorServiceFactory::class,
            Service\DifferentielService::class              => Service\Factory\DifferentielServiceFactory::class,
            Service\SourceService::class                    => Service\Factory\SourceServiceFactory::class,
            Service\TableService::class                     => Service\Factory\TableServiceFactory::class,
            Processus\ImportProcessus::class                => Processus\Factory\ImportProcessusFactory::class,
            ORM\Event\Listeners\EntitySourceInjector::class => ORM\Event\Listeners\Factory\EntitySourceInjectorFactory::class,
            Command\MajVuesFonctionsCommand::class          => Command\MajVuesFonctionsCommandFactory::class,
            Command\SyncJobCommand::class                   => Command\SyncJobCommandFactory::class,
        ],
    ],

    'form_elements' => [
        'factories' => [
            Form\SourceForm::class => Form\Factory\SourceFormFactory::class,
            Form\TableForm::class  => Form\Factory\TableFormFactory::class,
        ],
    ],

    'view_helpers' => [
        'factories' => [
            'differentielListe' => View\Helper\Factory\DifferentielListeViewHelperFactory::class,
            'differentielLigne' => View\Helper\DifferentielLigne\Factory\DifferentielLigneViewHelperFactory::class,
        ],
    ],

    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];