CHANGELOG
=========

6.0.6 (20/02/2025)
------------------

- [Fix] Possibilité de préciser l'entité à utiliser pour le diff. de Ligne


6.0.5 (09/01/2025)
------------------

- Compatibilité Ok UnicaenPrivilege 7.0

6.0.4 (20/11/2024)
------------------

- [Fix] Typage de getSource


6.0.3 (07/11/2024)
------------------

- Migration de laminas/console vers synfony/console


6.0.2 (29/11/2023)
------------------
- [FIX] correction de bug à l'affichage du différentiel avec présence de filtre

6.0.1 (21/03/2023)
------------------
- Dép Unicaen/Authentification et Unicaen/Privilege à la place d'Unicaen/Auth


6.0.0 (03/02/2023)
------------------
- Dep unicaen 6.0.*

4.0.3 (10/12/2021)
------------------
- Dep UnicaenApp 4.0.*

4.0.2 (30/11/2021)
------------------
- Utilisation d'Unicaen 4 minimum

4.0.1 (24/11/2021)
------------------
- Mise à la norme PSR 4

4.0 (23/11/2021)
------------------
- Migration vers Laminas


3.1.7 (16/11/2021)
------------------
- Possibilité de personnaliser l'URL pour utiliser ACE via le parametre de config js_ace_url (par défaut rien ne change)

3.1.6 (04/10/2021)
------------------
- Suppression de la dépendance inutile à UnicaenOracle

3.1.5 (22/02/2021)
------------------
- Possibilité de MAJ des vues diff & les procédures de MAJ depuis la ligne de commande

3.1.4 (17/02/2021)
------------------
- Petite amélioration de l'affichage des différentiels

3.1.3 (16/02/2021)
------------------
- Affichage + précis du diff si autre que update

3.1.2 (16/02/2021)
------------------
- Par défaut, IntervenantProcessus->execMaj réutilise les filtres positionnés dans la config des tables
- Msg d'avertissement pour les OR sur les filtres

3.1.1 (11/02/2021)
------------------
- Possibilité d'afficher un diff d'import sans tenir compte du filtre global
- Ajout d'un filtre pour ne pas supprimer des données saisies dans OSE même si les données locales sont synchronisables

3.1.0 (29/01/2021)
------------------
- L'opération de synchro peut maintenant gérer le changement de source d'une donnée (SOURCE_ID qui change).
- Si la colonne ID est fournie dans la vue source, alors cette dernière est utilisée pour faire la jointure avec la table 
dans la vue différentielle. Le SOURCE_CODE est alors inexploité, même s'il doit être fourni tout de même.
- Possibilité de préciser si les données non importables (CAD saisies directement dans l'application) peuvent devenir synchronisables si elles sont par la suite remontées par la vue source.
- Possibilité de définir plusieurs champs autres que SOURCE_CODE et ANNEE_ID pour définir la clé de correspondance entre vues sources et tables.

3.0.2 (16/06/2020)
------------------
Prise en compte des associations (clés étrangères) pour pouvoir retrouver le nom de la colonne
