<?php

namespace UnicaenImport\Entity\Schema;


/**
 *
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
class Column
{

    /**
     * @var string
     */
    public $table;

    /**
     * @var string
     */
    public $name;

    /**
     * Type de données
     *
     * @var string
     */
    public $dataType;

    /**
     * Longueur
     *
     * @var integer
     */
    public $length;

    /**
     * Nullable
     *
     * @var boolean
     */
    public $nullable;

    /**
     * Si la colonne possède ou non une valeur par défaut
     *
     * @var boolean
     */
    public $hasDefault;

    /**
     * Nom de la table référence (si clé étrangère)
     *
     * @var string
     */
    public $refTableName;

    /**
     * Nom du champ référence (si clé étrangère)
     *
     * @var string
     */
    public $refColumnName;

    /**
     * Si l'import par synchronisation est actif ou non
     *
     * @var boolean
     */
    public $importActif;

    /**
     * Si la colonne est une clé pour l'import ou non
     *
     * @var boolean
     */
    public $isKey = false;



    public function isCompleteKey()
    {
        return $this->isKey || $this->name == 'SOURCE_CODE' || ($this->importActif && $this->name == 'ANNEE_ID');
    }



    public function __toString()
    {
        return $this->name;
    }

}