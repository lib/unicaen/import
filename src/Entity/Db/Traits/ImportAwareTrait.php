<?php

namespace UnicaenImport\Entity\Db\Traits;

trait ImportAwareTrait
{
    use SourceAwareTrait;

    /**
     * @var string
     */
    protected $sourceCode;



    /**
     * @param string $sourceCode
     *
     * @return self
     */
    public function setSourceCode($sourceCode)
    {
        $this->sourceCode = $sourceCode;

        return $this;
    }



    /**
     * @return string
     */
    public function getSourceCode()
    {
        return $this->sourceCode;
    }
}