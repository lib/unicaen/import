<?php

namespace UnicaenImport\Entity\Db;

use Doctrine\ORM\Mapping as ORM;

/**
 * Table
 *
 * @ORM\Entity
 * @ORM\Table(name="IMPORT_TABLES")
 */
class Table
{

    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(name="TABLE_NAME", type="string", length=30, unique=true, nullable=false)
     */
    protected $name;

    /**
     * @var integer
     * @ORM\Column(name="ORDRE", type="integer", nullable=true)
     */
    protected $ordre;

    /**
     * @var string|null
     */
    protected $keyColumns;

    /**
     * @var bool
     * @ORM\Column(name="SYNC_ENABLED", type="boolean", nullable=false)
     */
    protected $syncEnabled = false;

    /**
     * @var string|null
     * @ORM\Column(name="SYNC_FILTRE", type="string", length=2000, nullable=true)
     */
    protected $syncFiltre = null;

    /**
     * @var string|null
     * @ORM\Column(name="SYNC_JOB", type="string", length=40, nullable=true)
     */
    protected $syncJob = null;

    /**
     * @var string|null
     * @ORM\Column(name="SYNC_HOOK_BEFORE", type="string", length=4000, nullable=true)
     */
    protected $syncHookBefore = null;

    /**
     * @var string|null
     * @ORM\Column(name="SYNC_HOOK_AFTER", type="string", length=4000, nullable=true)
     */
    protected $syncHookAfter = null;

    /**
     * @var bool
     * @ORM\Column(name="SYNC_NON_IMPORTABLES", type="boolean", nullable=false)
     */
    protected $syncNonImportables = false;



    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }



    /**
     * @param string $name
     *
     * @return Table
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }



    /**
     * @return int
     */
    public function getOrdre(): int
    {
        return $this->ordre;
    }



    /**
     * @param int $ordre
     *
     * @return Table
     */
    public function setOrdre(int $ordre): Table
    {
        $this->ordre = $ordre;

        return $this;
    }



    /**
     * @return string|null
     */
    public function getKeyColumns(): ?string
    {
        return $this->keyColumns;
    }



    /**
     * @param string|null $keyColumns
     *
     * @return Table
     */
    public function setKeyColumns(?string $keyColumns): Table
    {
        $this->keyColumns = $keyColumns;

        return $this;
    }





    /**
     * @return bool
     */
    public function isSyncEnabled(): bool
    {
        return $this->syncEnabled;
    }



    /**
     * @param bool $syncEnabled
     *
     * @return Table
     */
    public function setSyncEnabled(bool $syncEnabled): Table
    {
        $this->syncEnabled = $syncEnabled;

        return $this;
    }



    /**
     * @return string
     */
    public function getSyncFiltre()
    {
        return $this->syncFiltre;
    }



    /**
     * @param string $syncFiltre
     *
     * @return Table
     */
    public function setSyncFiltre($syncFiltre)
    {
        $this->syncFiltre = $syncFiltre;

        return $this;
    }



    /**
     * @return null|string
     */
    public function getSyncJob()
    {
        return $this->syncJob;
    }



    /**
     * @param null|string $syncJob
     *
     * @return Table
     */
    public function setSyncJob($syncJob): Table
    {
        $this->syncJob = $syncJob;

        return $this;
    }



    /**
     * @return null|string
     */
    public function getSyncHookBefore()
    {
        return $this->syncHookBefore;
    }



    /**
     * @param null|string $syncHookBefore
     *
     * @return Table
     */
    public function setSyncHookBefore($syncHookBefore): Table
    {
        $this->syncHookBefore = $syncHookBefore;

        return $this;
    }



    /**
     * @return null|string
     */
    public function getSyncHookAfter()
    {
        return $this->syncHookAfter;
    }



    /**
     * @param null|string $syncHookAfter
     *
     * @return Table
     */
    public function setSyncHookAfter($syncHookAfter): Table
    {
        $this->syncHookAfter = $syncHookAfter;

        return $this;
    }



    /**
     * @return bool
     */
    public function getSyncNonImportables(): bool
    {
        return $this->syncNonImportables;
    }



    /**
     * @param bool $syncNonImportables
     *
     * @return Table
     */
    public function setSyncNonImportables(bool $syncNonImportables): Table
    {
        $this->syncNonImportables = $syncNonImportables;

        return $this;
    }
    
}