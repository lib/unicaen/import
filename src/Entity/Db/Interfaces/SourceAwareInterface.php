<?php

namespace UnicaenImport\Entity\Db\Interfaces;

use UnicaenImport\Entity\Db\Source;

/**
 * Interface des entités possédant une "source".
 *
 * @see Source
 */
interface SourceAwareInterface
{
    /**
     * @param Source $source
     *
     * @return $this
     */
    public function setSource(Source $source = null);



    /**
     * @return Source
     */
    public function getSource();
}