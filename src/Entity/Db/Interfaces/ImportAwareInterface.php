<?php

namespace UnicaenImport\Entity\Db\Interfaces;

use UnicaenImport\Entity\Db\Source;

/**
 * Interface des entités possédant une gestion de l'import.
 *
 * @see Source
 */
interface ImportAwareInterface
{
    public function setSource(Source $source = null);



    /**
     * @return Source
     */
    public function getSource(): ?Source;



    /**
     * @param string $sourceCode
     *
     * @return $this
     */
    public function setSourceCode($sourceCode);



    /**
     * @return string
     */
    public function getSourceCode();
}