<?php

namespace UnicaenImport\Processus\Traits;

use UnicaenImport\Processus\ImportProcessus;
use RuntimeException;

/**
 * Description of ImportProcessusAwareTrait
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
trait ImportProcessusAwareTrait
{
    /**
     * @var ImportProcessus
     */
    private $processusImport;



    /**
     * @param ImportProcessus $processusImport
     *
     * @return self
     */
    public function setProcessusImport(ImportProcessus $processusImport)
    {
        $this->processusImport = $processusImport;

        return $this;
    }



    /**
     * @return ImportProcessus
     * @throws RuntimeException
     */
    public function getProcessusImport()
    {
        return $this->processusImport;
    }
}