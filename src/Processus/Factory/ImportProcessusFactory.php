<?php

namespace UnicaenImport\Processus\Factory;

use UnicaenImport\Processus\ImportProcessus;
use UnicaenImport\Service\DifferentielService;
use UnicaenImport\Service\QueryGeneratorService;
use UnicaenImport\Service\TableService;
use Psr\Container\ContainerInterface;


/**
 * Description of ImportProcessusFactory
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class ImportProcessusFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return ImportProcessus
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        $processus = new ImportProcessus();
        $processus->setServiceQueryGenerator( $container->get(QueryGeneratorService::class));
        $processus->setServiceTable($container->get(TableService::class));

        return $processus;
    }
}