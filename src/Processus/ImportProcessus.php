<?php

namespace UnicaenImport\Processus;

use UnicaenImport\Entity\Differentiel\Query;
use UnicaenImport\Service\Traits\QueryGeneratorServiceAwareTrait;
use UnicaenImport\Service\Traits\TableServiceAwareTrait;


/**
 *
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class ImportProcessus
{
    use QueryGeneratorServiceAwareTrait;
    use TableServiceAwareTrait;

    /**
     * Mise à jour de l'existant uniquement
     */
    const A_UPDATE = 'update';

    /**
     * Insertion de nouvelles données ou restauration d'anciennes uniquement
     */
    const A_INSERT = 'insert';

    /**
     * Mise à jour globale
     */
    const A_ALL = 'all';



    /**
     * Mise à jour des vues différentielles et des paquetages de mise à jour des données
     *
     * @return self
     */
    public function updateViewsAndPackages()
    {
        $this->getServiceQueryGenerator()->updateViewsAndPackages();

        return $this;
    }



    /**
     * Construit et exécute la reqûete d'interrogation des vues différentielles
     *
     * @param string      $tableName Nom de la table
     * @param string      $name      Nom du champ à tester
     * @param string|null $value     Valeur de test du champ
     * @param string      $action    Action
     * @param bool        $useSyncFiltre Utilisation ou non du filtre par défaut                              
     *
     * @retun self
     */
    public function execMaj($tableName, $name, $value = null, $action = self::A_ALL, ?bool $useSyncFiltre = true)
    {
        if ('SOURCE_CODE' == $name && $value !== null) {
            $value = (string)$value;
        }
        $query = new Query($tableName);
        if ($useSyncFiltre) {
            $query->addDefaultSyncFiltre($this->getServiceQueryGenerator());
        }
        if (null !== $value) $query->addColValue($name, $value);
        switch ($action) {
            case 'insert':
                $query->setAction([Query::ACTION_INSERT, Query::ACTION_UNDELETE]);
            break;
            case 'update':
                $query->setAction([Query::ACTION_UPDATE, Query::ACTION_DELETE]);
            break;
        }
        $this->getServiceQueryGenerator()->execMaj($query);

        return $this;
    }



    public function syncJob(string $job)
    {
        $tables = $this->getServiceTable()->getList();

        foreach( $tables as $table ){
            if ($table->getSyncJob() == $job && $table->isSyncEnabled()){
                $this->getServiceQueryGenerator()->execMaj($table);
            }
        }
    }
}