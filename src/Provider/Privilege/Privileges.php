<?php

namespace UnicaenImport\Provider\Privilege;

/**
 * Description of Privileges
 *
 * Liste des privilèges utilisables dans votre application
 *
 * @author UnicaenCode
 */
class Privileges extends \UnicaenPrivilege\Provider\Privilege\Privileges
{
    const IMPORT_ECARTS                              = 'import-ecarts';
    const IMPORT_MAJ                                 = 'import-maj';
    const IMPORT_SOURCES_EDITION                     = 'import-sources-edition';
    const IMPORT_SOURCES_VISUALISATION               = 'import-sources-visualisation';
    const IMPORT_TABLES_EDITION                      = 'import-tables-edition';
    const IMPORT_TABLES_VISUALISATION                = 'import-tables-visualisation';
    const IMPORT_TBL                                 = 'import-tbl';
    const IMPORT_VUES_PROCEDURES                     = 'import-vues-procedures';
}