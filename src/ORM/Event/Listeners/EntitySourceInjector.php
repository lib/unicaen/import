<?php

namespace UnicaenImport\ORM\Event\Listeners;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use UnicaenImport\Entity\Db\Interfaces\ImportAwareInterface;
use UnicaenImport\Entity\Db\Interfaces\SourceAwareInterface;
use UnicaenImport\Entity\Db\Source;
use UnicaenImport\Options\Traits\ModuleOptionsAwareTrait;

/**
 * Listener Doctrine.
 *
 * Injecte automatiquement la source (resp. le source code) spécifiée dans la config
 * dans toute entité implémentant SourceAwareInterface (resp. ImportAwareInterface).
 *
 * @see    SourceAwareInterface
 * @see    ImportAwareInterface
 * @author Unicaen
 */
class EntitySourceInjector implements EventSubscriber
{
    use ModuleOptionsAwareTrait;



    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
        ];
    }



    /**
     * @return string
     */
    private function getSourceCode()
    {
        $options = $this->getOptionsModule();

        return $options->getEntitySourceInjector()['source_code'];
    }



    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        // Pas la peine de continuer si on a pas de code de la source à injecter.
        if ($this->getSourceCode() === null) {
            return;
        }

        // L'entité doit implémenter l'interface SourceAwareInterface.
        if (!($entity instanceof SourceAwareInterface || $entity instanceof ImportAwareInterface)) {
            return;
        }

        // Si une source est déjà renseignée, rien à faire.
        if ($entity->getSource() !== null) {
            return;
        }

        // Injection de la source
        /** @var Source $source */
        $source = $args->getEntityManager()->getRepository(Source::class)->findOneBy([
            'code' => $this->getSourceCode(),
        ]);
        $entity->setSource($source);

        // Injection si possible et si besoin du "sourceCode"
        if ($entity instanceof ImportAwareInterface && $entity->getSourceCode() === null) {
            // le "sourceCode" par défaut de l'entité est son id
            if (method_exists($entity, 'getId')) {
                $entity->setSourceCode($entity->getId());
            }
        }
    }
}