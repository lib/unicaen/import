<?php

namespace UnicaenImport\ORM\Event\Listeners\Factory;

use Psr\Container\ContainerInterface;
use UnicaenImport\Options\ModuleOptions;
use UnicaenImport\ORM\Event\Listeners\EntitySourceInjector;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

final class EntitySourceInjectorFactory implements FactoryInterface
{
    /**
     * @deprecated
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new EntitySourceInjector();
        $service->setOptionsModule($container->get(ModuleOptions::class));

        return $service;
    }
}