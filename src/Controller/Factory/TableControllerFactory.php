<?php

namespace UnicaenImport\Controller\Factory;

use Psr\Container\ContainerInterface;
use UnicaenImport\Controller\TableController;
use UnicaenImport\Form\TableForm;
use UnicaenImport\Options\ModuleOptions;
use UnicaenImport\Processus\ImportProcessus;
use UnicaenImport\Service\SchemaService;
use UnicaenImport\Service\TableService;


/**
 * Description of TableControllerFactory
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class TableControllerFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return TableController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        $controller = new TableController;
        $controller->setServiceTable($container->get(TableService::class));
        $controller->setServiceSchema($container->get(SchemaService::class));
        $controller->setFormTable($container->get('FormElementManager')->get(TableForm::class));
        $controller->setProcessusImport($container->get(ImportProcessus::class));
        $controller->setOptionsModule($container->get(ModuleOptions::class));

        return $controller;
    }
}