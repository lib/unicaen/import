<?php

namespace UnicaenImport\Controller\Factory;

use Psr\Container\ContainerInterface;
use UnicaenImport\Form\SourceForm;
use UnicaenImport\Service\SourceService;
use UnicaenImport\Controller\SourceController;

/**
 * Description of SourceControllerFactory
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class SourceControllerFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return SourceController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        $controller = new SourceController;
        $controller->setServiceSource($container->get(SourceService::class));
        $controller->setFormSource($container->get('FormElementManager')->get(SourceForm::class));

        return $controller;
    }
}