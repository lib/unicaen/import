<?php

namespace UnicaenImport\Controller\Factory;

use Psr\Container\ContainerInterface;
use UnicaenImport\Controller\DifferentielController;
use UnicaenImport\Service\DifferentielService;
use UnicaenImport\Service\QueryGeneratorService;
use UnicaenImport\Service\SchemaService;

/**
 * Description of DifferentielControllerFactory
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class DifferentielControllerFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return DifferentielController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        $controller = new DifferentielController();
        $controller->setServiceSchema($container->get(SchemaService::class));
        $controller->setServiceQueryGenerator($container->get(QueryGeneratorService::class));
        $controller->setServiceDifferentiel($container->get(DifferentielService::class));

        return $controller;
    }
}