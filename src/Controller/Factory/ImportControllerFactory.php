<?php

namespace UnicaenImport\Controller\Factory;

use Psr\Container\ContainerInterface;
use UnicaenImport\Controller\ImportController;
use UnicaenImport\Processus\ImportProcessus;
use UnicaenImport\Service\SchemaService;

/**
 * Description of ImportControllerFactory
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class ImportControllerFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return PlafondController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        $controller = new ImportController();
        $controller->setServiceSchema($container->get(SchemaService::class));
        $controller->setProcessusImport($container->get(ImportProcessus::class));

        return $controller;
    }
}