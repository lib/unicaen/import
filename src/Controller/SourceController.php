<?php

namespace UnicaenImport\Controller;


use UnicaenApp\View\Model\MessengerViewModel;
use UnicaenImport\Entity\Db\Source;
use UnicaenImport\Form\Traits\SourceFormAwareTrait;
use UnicaenImport\Service\Traits\SourceServiceAwareTrait;
use Laminas\Mvc\Controller\AbstractActionController;


/**
 * Description of SourceController
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class SourceController extends AbstractActionController
{
    use SourceServiceAwareTrait;
    use SourceFormAwareTrait;

    public function indexAction()
    {
        $sources = $this->getServiceSource()->getList();

        return compact('sources');
    }



    public function editionAction()
    {
        $sourceId = $this->params()->fromRoute('source');
        $source   = $this->getServiceSource()->get($sourceId);

        $form = $this->getFormSource();
        if (empty($source)) {
            $title = 'Création d\'une nouvelle source';
            $source  = new Source();
        } else {
            $title = 'Modification de la source';

        }
        $form->bind($source);
        $form->setAttribute('action', $this->url()->fromRoute(null, [], [], true));

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                try {
                    $this->getServiceSource()->save($source);
                } catch (\Exception $e) {
                    $this->flashMessenger()->addErrorMessage($e->getMessage());
                }
            }
        }

        return compact('form', 'title');
    }



    public function suppressionAction()
    {
        $sourceId = $this->params()->fromRoute('source');
        $source   = $this->getServiceSource()->get($sourceId);

        try {
            $this->getServiceSource()->delete($source);
            $this->flashMessenger()->addSuccessMessage("Source supprimée avec succès.");
        } catch (\Exception $e) {
            $this->flashMessenger()->addErrorMessage($e->getMessage());
        }

        return new MessengerViewModel();
    }
}