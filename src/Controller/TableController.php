<?php

namespace UnicaenImport\Controller;

use UnicaenImport\Form\Traits\TableFormAwareTrait;
use UnicaenImport\Options\Traits\ModuleOptionsAwareTrait;
use UnicaenImport\Processus\Traits\ImportProcessusAwareTrait;
use UnicaenImport\Service\Traits\SchemaServiceAwareTrait;
use UnicaenImport\Service\Traits\TableServiceAwareTrait;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\JsonModel;


/**
 * Description of TableController
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class TableController extends AbstractActionController
{
    use TableServiceAwareTrait;
    use SchemaServiceAwareTrait;
    use TableFormAwareTrait;
    use ImportProcessusAwareTrait;
    use ModuleOptionsAwareTrait;



    public function indexAction()
    {
        $tables           = $this->getServiceTable()->getList();
        $views            = $this->getServiceSchema()->getImportViews();
        $hasNonReferenced = !empty($this->getServiceSchema()->getNonReferencedImportTables());
        $jsAceUrl         = $this->getOptionsModule()->getJsAceUrl();
        
        return compact('tables', 'hasNonReferenced', 'views', 'jsAceUrl');
    }



    public function addNonReferencedAction()
    {
        $nonReferenced = $this->getServiceSchema()->getNonReferencedImportTables();
        $this->getServiceTable()->addTables($nonReferenced);

        return new JsonModel(['result' => count($nonReferenced) . ' tables ont été nouvellement référencées']);
    }



    public function synchroSwitchAction()
    {
        $tableName = $this->params()->fromPost('table');

        $table = $this->getServiceTable()->get($tableName);
        if ($table) {
            $table->setSyncEnabled(!$table->isSyncEnabled());
            $this->getServiceTable()->save($table);
            $this->getProcessusImport()->updateViewsAndPackages();
        }

        $result = [
            'table'  => $table->getName(),
            'result' => $table->isSyncEnabled(),
        ];

        return new JsonModel($result);
    }



    public function trierAction()
    {
        $tables = $this->params()->fromPost('tables');
        $this->getServiceTable()->trier($tables);

        return new JsonModel(['result' => true]);
    }



    public function modifierAction()
    {
        $tableName = $this->params()->fromRoute('table');
        $table     = $this->getServiceTable()->get($tableName);

        $form  = $this->getFormTable();
        $title = 'Synchronisation de '.$tableName;
        $form->bind($table);
        $form->setAttribute('action', $this->url()->fromRoute(null, [], [], true));

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                try {
                    $this->getServiceTable()->save($table);
                    $this->getProcessusImport()->updateViewsAndPackages();
                } catch (\Exception $e) {
                    $this->flashMessenger()->addErrorMessage($e->getMessage());
                }
            }
        }

        return compact('form', 'title');
    }
}