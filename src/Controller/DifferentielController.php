<?php

namespace UnicaenImport\Controller;

use UnicaenImport\Entity\Differentiel\Query;
use UnicaenImport\Provider\Privilege\Privileges;
use UnicaenImport\Service\Traits\DifferentielServiceAwareTrait;
use UnicaenImport\Service\Traits\QueryGeneratorServiceAwareTrait;
use UnicaenImport\Service\Traits\SchemaServiceAwareTrait;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\JsonModel;


/**
 * Description of DifferentielController
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class DifferentielController extends AbstractActionController
{
    use SchemaServiceAwareTrait;
    use QueryGeneratorServiceAwareTrait;
    use DifferentielServiceAwareTrait;



    public function indexAction()
    {
        $sc = $this->getServiceSchema();

        $tables = $sc->getImportTables();
        $mviews = $sc->getImportMviews();

        return compact('tables', 'mviews');
    }



    public function synchronisationAction()
    {
        $errors  = [];
        $success = [];

        $table  = $this->params()->fromRoute('table');
        $action = $this->params()->fromPost('action');

        /* Mise à jour */
        if ($this->isAllowed(Privileges::getResourceId(Privileges::IMPORT_MAJ))) {
            if ('vue-materialisee' === $action) {
                try {
                    $sq = $this->getServiceQueryGenerator();
                    $sq->execMajVM($table);
                    $success[] = 'Vue matérialisée mise à jour avec succès';
                } catch (\Exception $e) {
                    $errors = [$e->getMessage()];
                }
            } elseif ($action) {
                $sq    = $this->getServiceQueryGenerator();
                $query = new Query($table);
                $query->addDefaultSyncFiltre($sq);
                if ($action != 'all') $query->setAction($action);

                /* Mise à jour des données et récupération des éventuelles erreurs */
                try {
                    $errors = $sq->syncTable($query);
                    if (!$errors) {
                        $success[] = 'Synchronisation effectuée avec succès';
                    }
                } catch (\Exception $e) {
                    $errors = [$e->getMessage()];
                }
            }
        } else {
            if ($action) {
                $errors[] = 'Vous n\'avez pas le droit de synchroniser cette table';
            }
        }

        /* Récupération et rendu final */
        $query    = new Query($table);
        $chiffres = $this->getServiceDifferentiel()->make($query, Query::SQL_SUMMARY)->fetchSummary();

        return new JsonModel(compact('table', 'chiffres', 'errors', 'success'));
    }



    public function detailsAction()
    {
        $table  = $this->params()->fromRoute('table');
        $action = $this->params()->fromPost('action');

        $sc = $this->getServiceSchema();

        $query = new Query($table);
        if ($action != 'all') $query->setAction($action);
        $query->setNotNull([]); // Aucune colonne ne doit être non nulle !!
        $query->setLimit(101);
        $lignes = $this->getServiceDifferentiel()->make($query, $query::SQL_FULL)->fetchAll();

        return compact('lignes');
    }
}