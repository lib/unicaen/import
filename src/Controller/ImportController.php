<?php

namespace UnicaenImport\Controller;

use UnicaenImport\Processus\Traits\ImportProcessusAwareTrait;
use UnicaenImport\Service\Traits\SchemaServiceAwareTrait;
use Laminas\Mvc\Controller\AbstractActionController;

/**
 *
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class ImportController extends AbstractActionController
{
    use SchemaServiceAwareTrait;
    use ImportProcessusAwareTrait;

    public function indexAction()
    {
        return [];
    }


    public function tableauBordAction()
    {
        $data = $this->getServiceSchema()->getSchema();

        return compact('data');
    }



    public function majVuesFonctionsAction()
    {
        try {
            $this->getProcessusImport()->updateViewsAndPackages();
            $message = 'Mise à jour des vues différentielles et du paquetage d\'import terminés';
        } catch (\Exception $e) {
            $message = 'Une erreur a été rencontrée.';
            throw new \UnicaenApp\Exception\LogicException("import impossible", null, $e);
        }
        $title = "Résultat";

        return compact('message', 'title');
    }
}