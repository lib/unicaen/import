<?php

namespace UnicaenImport\Service;

use Doctrine\DBAL\Driver\Statement;
use UnicaenImport\Entity\Differentiel\Ligne;
use UnicaenImport\Entity\Differentiel\Query;
use UnicaenImport\Service\Traits\QueryGeneratorServiceAwareTrait;


/**
 * Classe permettant de récupérer le différentiel entre une table source et une table de l'application.
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
class DifferentielService extends AbstractService
{
    use QueryGeneratorServiceAwareTrait;

    /**
     * Statement
     *
     * @var Statement
     */
    protected $stmt;

    /**
     * Nom de la table courante
     *
     * @var string
     */
    protected $tableName;



    /**
     * Construit un différentiel
     *
     * @param string $query Requête de filtrage
     *
     * @return self
     */
    public function make(Query $query, $mode = Query::SQL_PARTIAL, bool $noDefaultSyncFiltre=false)
    {
        $this->tableName = $query->getTableName();
        if (!$noDefaultSyncFiltre) {
            $query->addDefaultSyncFiltre($this->getServiceQueryGenerator());
        }
        $this->stmt = $this->getEntityManager()->getConnection()->executeQuery($query->toSql($mode), []);

        return $this;
    }



    /**
     * Récupère la prochaine ligne de différentiel
     *
     * @return Ligne|false
     */
    public function fetchNext()
    {
        $data = $this->stmt->fetch();
        if ($data) return new Ligne($this->getEntityManager(), $this->tableName, $data);

        return false;
    }



    /**
     * Retourne toutes les lignes concernées
     *
     * @return Ligne[]
     */
    public function fetchAll()
    {
        $result = [];
        while ($data = $this->stmt->fetch()) {
            if ($data) $result[] = new Ligne($this->getEntityManager(), $this->tableName, $data);
        }

        return $result;
    }



    public function fetchSummary()
    {
        $result = [
            Query::ACTION_INSERT   => 0,
            Query::ACTION_UPDATE   => 0,
            Query::ACTION_DELETE   => 0,
            Query::ACTION_UNDELETE => 0,
        ];
        while ($data = $this->stmt->fetch()) {
            if ($data) $result[$data['IMPORT_ACTION']] = (int)$data['NB'];
        }

        return $result;
    }
}