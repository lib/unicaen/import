<?php

namespace UnicaenImport\Service;

use UnicaenApp\Service\EntityManagerAwareInterface;
use UnicaenApp\Service\EntityManagerAwareTrait;
use UnicaenImport\Entity\Db\Table;
use UnicaenImport\Entity\Differentiel\Query;

/**
 * Description of TableService
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class TableService implements EntityManagerAwareInterface
{
    use EntityManagerAwareTrait;



    /**
     * @return Table[]
     */
    public function getList()
    {
        return $this->getRepo()->findBy([], ['ordre' => 'ASC', 'name' => 'ASC']);
    }



    /**
     * @param integer $tableName
     *
     * @return null|Table
     */
    public function get($tableName)
    {
        if (!$tableName) return null;

        return $this->getRepo()->find($tableName);
    }



    /**
     * @param Table $table
     *
     * @return Table
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Table $table)
    {
        $this->getEntityManager()->persist($table);
        $this->getEntityManager()->flush($table);

        return $table;
    }



    public function delete(Table $table)
    {
        $this->getEntityManager()->remove($table);
        $this->getEntityManager()->flush($table);

        return $this;
    }



    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepo()
    {
        return $this->getEntityManager()->getRepository(Table::class);
    }



    /**
     * @param array $tables
     *
     * @return TableService
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addTables(array $tables): self
    {
        foreach( $tables as $tablename ){
            $table = new Table();
            $table->setName($tablename);
            $this->save($table);
        }

        return $this;
    }



    public function trier(array $tables)
    {
        $sql = "update import_tables set ordre = null";
        $this->getEntityManager()->getConnection()->exec($sql);
        $ordre = 1;
        foreach( $tables as $table){
            $sql = "update import_tables set ordre = :ordre";
            $this->getEntityManager()->getConnection()->update('IMPORT_TABLES', ['ORDRE' => $ordre], ['TABLE_NAME' => $table]);
            $ordre++;
        }
    }
}