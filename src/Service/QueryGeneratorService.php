<?php

namespace UnicaenImport\Service;

use Doctrine\DBAL\DBALException;
use UnicaenImport\Entity\Db\Table;
use UnicaenImport\Entity\Schema\Column;
use UnicaenImport\Exception\Exception;
use UnicaenImport\Entity\Differentiel\Query;
use UnicaenImport\Options\Traits\ModuleOptionsAwareTrait;
use UnicaenImport\Service\Traits\SchemaServiceAwareTrait;
use UnicaenImport\Service\Traits\TableServiceAwareTrait;

/**
 *
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
class QueryGeneratorService extends AbstractService
{
    use SchemaServiceAwareTrait;
    use TableServiceAwareTrait;
    use ModuleOptionsAwareTrait;

    const AUTOGEN_PACKAGE_NAME = 'UNICAEN_IMPORT_AUTOGEN_PROCS__';
    const AG_BEGIN             = '-- AUTOMATIC GENERATION --';
    const AG_END               = '-- END OF AUTOMATIC GENERATION --';
    const ANNEE_COLUMN_NAME    = 'ANNEE_ID';

    /**
     * Tables
     *
     * @var string[]
     */
    protected $tables;

    /**
     * Colonnes
     *
     * @var array
     */
    protected $cols = [];



    /**
     * Retourne la liste des tables importables
     *
     * @return string[]
     */
    protected function getTables()
    {
        if (empty($this->tables)) {
            $this->tables = $this->getServiceSchema()->getImportTables();
        }

        return $this->tables;
    }



    /**
     * Retourne la liste des colonnes importables d'une table
     *
     * @param string $tableName
     *
     * @return string[]
     */
    protected function getCols($tableName)
    {
        if (!isset($this->cols[$tableName])) {
            $this->cols[$tableName] = $this->getServiceSchema()->getImportCols($tableName);
        }

        return $this->cols[$tableName];
    }



    public function execMajVM($tableName)
    {
        $mviewName = $this->escape('MV_' . $tableName);
        $sql       = "BEGIN DBMS_MVIEW.REFRESH($mviewName, 'C'); END;";
        try {
            $this->getEntityManager()->getConnection()->exec($sql);
        } catch (\Doctrine\DBAL\DBALException $e) {
            throw Exception::duringMajMVException($e, $tableName);
        }
    }



    /**
     * Met à jour des données d'après la requête transmise
     *
     * @param Query|Table $data Requête de filtrage pour la mise à jour
     *
     * @retun self
     */
    public function execMaj($data)
    {
        switch (true) {
            case $data instanceof Query:
                $query  = $data;
                $before = null;
                $after  = null;
            break;
            case $data instanceof Table:
                $query  = new Query($data->getName());
                $before = $data->getSyncHookBefore();
                $after  = $data->getSyncHookAfter();
            break;
            default:
                throw new \Exception('Données transmises non gérables.');
        }

        $currentUser = $this->getDbUser();
        if (empty($currentUser)) {
            throw new Exception('Vous devez être authentifié pour réaliser cette action');
        }
        $userId     = $this->escape($currentUser->getId());
        $tableName  = $this->escape($query->getTableName());
        $conditions = $query->toSql(Query::SQL_PARTIAL);
        if (!empty($conditions)) {
            $conditions = $this->escape($conditions);
        } else {
            $conditions = 'NULL';
        }
        $ignoreFields = $query->getIgnoreFields();
        if (empty($ignoreFields)) {
            $ignoreFields = 'NULL';
        } else {
            $ignoreFields = $this->escape(implode(',', $ignoreFields));
        }

        $sql = "BEGIN\n"
            . "\t" . $this->getPackage() . ".SET_CURRENT_USER($userId);\n"
            . "\t" . $before . "\n"
            . "\t" . $this->getPackage() . ".SYNCHRONISATION($tableName,$conditions,$ignoreFields);\n"
            . "\t" . $after . "\n"
            . "END;";
        try {
            $this->getEntityManager()->getConnection()->exec($sql);
        } catch (DBALException $e) {
            throw Exception::duringMajException($e, $query->getTableName());
        }

        return $this;
    }



    /**
     * Synchronise une table
     *
     * @param Query $query
     *
     * @return string[]
     */
    public function syncTable(Query $query)
    {
        $errors    = [];
        $lastLogId = $this->getLastLogId();

        try {
            $this->execMaj($query);
        } catch (DBALException $e) {
            $errors[] = $e->getMessage();
        }
        $errors = $errors + $this->getLogMessages($lastLogId);

        return $errors;
    }



    /**
     * retourne le dernier ID du log de synchronisation
     *
     * @return int
     */
    protected function getLastLogId()
    {
        $sql  = "SELECT MAX(id) last_log_id FROM SYNC_LOG";
        $stmt = $this->getEntityManager()->getConnection()->executeQuery($sql);
        if ($r = $stmt->fetch()) {
            return (int)$r['LAST_LOG_ID'];
        }

        return 0;
    }



    /**
     * Retourne tous les messages d'erreur qui sont apparue depuis $since
     *
     * @param int $since
     *
     * @return string[]
     */
    protected function getLogMessages($since)
    {
        $since    = (int)$since;
        $sql      = "SELECT message FROM sync_log WHERE id > :since ORDER BY id";
        $messages = [];
        $stmt     = $this->getEntityManager()->getConnection()->executeQuery($sql, ['since' => (int)$since]);
        while ($r = $stmt->fetch()) {
            $messages[] = $r['MESSAGE'];
        }

        return $messages;
    }



    /**
     *
     * @param string $tableName
     *
     * @return null|string
     */
    public function getSyncFiltre($tableName)
    {
        $sql  = 'SELECT sync_filtre FROM import_tables WHERE table_name = :tableName';
        $stmt = $this->getEntityManager()->getConnection()->executeQuery($sql, compact('tableName'));

        if ($r = $stmt->fetch()) {
            $res = $r['SYNC_FILTRE'];
            if ($res) return $res; else return null;
        }

        return null;
    }



    /**
     * Mettre à jour toutes les infos dans la BDD
     *
     * @return self
     */
    public function updateViewsAndPackages()
    {
        $this->populateImportTables();

        $views = $this->makeDiffViews();

        foreach ($views as $vn => $view) {
            $this->exec($view);
        }

        $declaration = $this->makePackageDeclaration();
        $this->exec($declaration);

        $body = $this->makePackageBody();
        $this->exec($body);

        return $this;
    }



    protected function getPackage()
    {
        return $this->getOptionsModule()->getPackage();
    }



    public function populateImportTables()
    {
        $tables = $this->getTables();
        foreach ($tables as $table) {
            $this->populateImportTable($table);
        }

        return $this;
    }



    protected function populateImportTable($tableName)
    {
        $repo  = $this->getEntityManager()->getRepository(Table::class);
        $table = $repo->findOneBy(['name' => $tableName]);
        if (!$table) {
            $table = new Table;
            $table->setName($tableName);

            $this->getEntityManager()->persist($table);
            $this->getEntityManager()->flush($table);
        }

        return $this;
    }



    /**
     * Construit toutes les vues différentielles
     *
     * @return array
     */
    protected function makeDiffViews()
    {
        $tables = $this->getTables();
        $result = [];
        foreach ($tables as $table) {
            $result[$table] = $this->makeDiffView($table);
        }

        return $result;
    }



    /**
     * Construit toutes les déclarations de procédures
     *
     * @return array
     */
    protected function makeProcDeclarations()
    {
        $tables = $this->getTables();
        $result = [];
        foreach ($tables as $table) {
            $result[$table] = $this->makeProcDeclaration($table);
        }

        return $result;
    }



    /**
     * Construit tous les corps de procédures
     *
     * @return array
     */
    protected function makeProcBodies()
    {
        $tables = $this->getTables();
        $result = [];
        foreach ($tables as $table) {
            $this->assertSequenceExists($table);
            $result[$table] = $this->makeProcBody($table);
        }

        return $result;
    }



    /**
     * Teste que la séquence correspondant à la table spécifiée existe bien.
     *
     * @param string $table
     *
     * @throws \Doctrine\DBAL\DBALException En cas d'erreur en BDD
     * @throws Exception Si la séquence n'existe pas.
     *
     */
    protected function assertSequenceExists($table)
    {
        $sql = 'SELECT COUNT(*) SEQ_FOUND FROM USER_SEQUENCES WHERE SEQUENCE_NAME = :sequenceName';

        $sequenceName = strtoupper($table) . '_ID_SEQ';
        $stmt         = $this->getEntityManager()->getConnection()->executeQuery($sql, ['sequenceName' => $sequenceName]);

        $sequenceFound = false;
        if ($r = $stmt->fetch()) {
            $sequenceFound = (bool)(int)$r['SEQ_FOUND'];
        }
        if (!$sequenceFound) {
            throw new Exception("La séquence '$sequenceName' doit être créée (ex: CREATE SEQUENCE $sequenceName)");
        }
    }



    /**
     * Constuit la nouvelle déclaration du package IMPORT
     *
     * @return string
     */
    protected function makePackageDeclaration()
    {
        $pname = self::AUTOGEN_PACKAGE_NAME;
        $decls = implode("\n", $this->makeProcDeclarations());

        $psql = "CREATE OR REPLACE PACKAGE $pname IS\n\n$decls\n\nEND $pname;";

        return $psql;
    }



    /**
     * Constuit la nouvelle déclaration du package IMPORT
     *
     * @return string
     */
    protected function makePackageBody()
    {
        $pname = self::AUTOGEN_PACKAGE_NAME;
        $procs = implode("\n\n\n\n", $this->makeProcBodies());

        $funcs = "
  FUNCTION IN_COLUMN_LIST( VALEUR VARCHAR2, CHAMPS CLOB ) RETURN NUMERIC IS
  BEGIN
    IF REGEXP_LIKE(CHAMPS, '(^|,)[ \\t\\r\\n\\v\\f]*' || VALEUR || '[ \\t\\r\\n\\v\\f]*(,|$)') THEN RETURN 1; END IF;
    RETURN 0;
  END;";

        $psql = "CREATE OR REPLACE PACKAGE BODY $pname IS\n$funcs\n\n\n$procs\n\nEND $pname;";

        return $psql;
    }



    /**
     * Génère une vue différentielle pour une table donnée
     *
     * @param string $tableName
     *
     * @return string
     */
    protected function makeDiffView($tableName)
    {
        // Pour l'annualisation :
        $schema = $this->getServiceSchema()->getSchema($tableName);
        $table  = $this->getServiceTable()->get($tableName);

        $sourceCodeCol = 'SOURCE_CODE';
        if ($this->getServiceSchema()->hasColumn('SRC_' . $tableName, 'ID')) {
            $sourceCodeCol = 'ID';
        }
        $joinCond = "S.$sourceCodeCol = D.$sourceCodeCol";
        $delCond  = '';
        $depJoin  = '';
        if ('ID' != $sourceCodeCol) {
            foreach ($schema as $columnName => $column) {
                if ($column->isKey) {
                    $joinCond .= ' AND S.' . $columnName . ' = d.' . $columnName;
                }
            }
        }
        if (array_key_exists(self::ANNEE_COLUMN_NAME, $schema)) {
            // Si la table courante est annualisée ...
            if ('ID' != $sourceCodeCol && $this->getServiceSchema()->hasColumn('V_DIFF_' . $tableName, self::ANNEE_COLUMN_NAME)) {
                // ... et que la source est également annualisée alors concordance nécessaire
                $joinCond .= ' AND S.' . self::ANNEE_COLUMN_NAME . ' = d.' . self::ANNEE_COLUMN_NAME;
            }
            // destruction ssi dans l'année d'import courante
            $delCond = ' AND d.' . self::ANNEE_COLUMN_NAME . ' >= ' . $this->getPackage() . '.get_current_annee';
        } else {
            // on recherche si la table dépend d'une table qui, elle, serait annualisée
            foreach ($schema as $columnName => $column) {
                /* @var $column \UnicaenImport\Entity\Schema\Column */
                if (!empty($column->refTableName)) {
                    $refSchema = $this->getServiceSchema()->getSchema($column->refTableName);
                    if (!empty($refSchema) && array_key_exists(self::ANNEE_COLUMN_NAME, $refSchema)) {
                        // Oui, la table dépend d'une table annualisée!!
                        // Donc, on utilise la table référente
                        $depJoin = "\n  LEFT JOIN " . $column->refTableName . " rt ON rt." . $column->refColumnName . " = d." . $columnName;
                        // destruction ssi dans l'année d'import courante de la table référente
                        $delCond = ' AND rt.' . self::ANNEE_COLUMN_NAME . ' >= ' . $this->getPackage() . '.get_current_annee';

                        break;
                        /* on stoppe à la première table contenant une année.
                         * S'il en existe une autre tant pis pour elle,
                         * les années doivent de toute manière être concordantes entres sources!!!
                         */
                    }
                }
            }
        }

        if ($this->getServiceSchema()->hasColumn('SRC_' . $tableName, 'ID')) {
            $joinCond = 'S.ID = D.ID';
        }

        // on génère ensuite la bonne requête !!!
        $cols = $this->getCols($tableName);
        foreach ($cols as $id => $col) {
            $cols[$id] = $schema[$col];
        }

        $sio = !$table->getSyncNonImportables();

        $sql = "CREATE OR REPLACE FORCE VIEW V_DIFF_$tableName AS
select diff.* from (SELECT
  D.id id,
  COALESCE( S.source_code, D.source_code ) source_code,
  CASE
    WHEN S.source_code IS NOT NULL AND D.source_code IS NULL THEN 'insert'
    WHEN S.source_code IS NOT NULL AND D.source_code IS NOT NULL AND (D.histo_destruction IS NULL OR D.histo_destruction > SYSDATE) THEN 'update'
    WHEN S.source_code IS NULL AND D.source_code IS NOT NULL AND (D.histo_destruction IS NULL OR D.histo_destruction > SYSDATE)$delCond THEN 'delete'
    WHEN S.source_code IS NOT NULL AND D.source_code IS NOT NULL AND D.histo_destruction IS NOT NULL AND D.histo_destruction <= SYSDATE THEN 'undelete' END import_action,
  " . $this->formatColQuery($cols, function (Column $col) {
                if ($col->dataType == 'CLOB') {
                    return 'CASE WHEN S.source_code IS NULL AND D.source_code IS NOT NULL THEN D.:column ELSE to_clob(S.:column) END :column';
                } else {
                    return 'CASE WHEN S.source_code IS NULL AND D.source_code IS NOT NULL THEN D.:column ELSE S.:column END :column';
                }
            }, ",\n  ") . ",
  " . $this->formatColQuery($cols, function (Column $col) {
                if ($col->dataType == 'CLOB') {
                    return 'CASE WHEN dbms_lob.compare(D.:column,S.:column) <> 0 OR (D.:column IS NULL AND S.:column IS NOT NULL) OR (D.:column IS NOT NULL AND S.:column IS NULL) THEN 1 ELSE 0 END U_:column';
                } else {
                    return 'CASE WHEN D.:column <> S.:column OR (D.:column IS NULL AND S.:column IS NOT NULL) OR (D.:column IS NOT NULL AND S.:column IS NULL) THEN 1 ELSE 0 END U_:column';
                }
            }, ",\n  ") . "
FROM
  $tableName D$depJoin
  FULL JOIN SRC_$tableName S ON $joinCond
  " . ($sio ? 'LEFT JOIN source ds ON ds.id = d.source_id' : '') . "
WHERE
  " . ($sio ? 'COALESCE(ds.importable,1) = 1 AND (' : '') . "
     (S.source_code IS NOT NULL AND D.source_code IS NOT NULL AND D.histo_destruction IS NOT NULL AND D.histo_destruction <= SYSDATE)
  OR (S.source_code IS NULL AND D.source_code IS NOT NULL AND (D.histo_destruction IS NULL OR D.histo_destruction > SYSDATE))
  OR (S.source_code IS NOT NULL AND D.source_code IS NULL)
  OR " . $this->formatColQuery($cols, function (Column $col) {
                if ($col->dataType == 'CLOB') {
                    return 'dbms_lob.compare(D.:column,S.:column) <> 0 OR (D.:column IS NULL AND S.:column IS NOT NULL) OR (D.:column IS NOT NULL AND S.:column IS NULL)';
                } else {
                    return 'D.:column <> S.:column OR (D.:column IS NULL AND S.:column IS NOT NULL) OR (D.:column IS NOT NULL AND S.:column IS NULL)';
                }
            }, "\n  OR ") . "
" . ($sio ? ')' : '') . ") diff WHERE import_action IS NOT NULL";

        return $sql;
    }



    /**
     * Génère une déclaration de procédure pour une table donnée
     *
     * @param string $tableName
     *
     * @return string
     */
    protected function makeProcDeclaration($tableName)
    {
        return "  PROCEDURE $tableName;";
    }



    /**
     * Génère un corps de procédure pour une table donnée
     *
     * @param string $tableName
     *
     * @return string
     */
    protected function makeProcBody($tableName)
    {
        $cols = $this->getCols($tableName);

        $sql = "  PROCEDURE $tableName IS
    TYPE r_cursor IS REF CURSOR;
    sync_filtre VARCHAR2(2000) DEFAULT '';
    sql_query   CLOB;
    diff_cur    r_cursor;
    diff_row    V_DIFF_$tableName%ROWTYPE;
  BEGIN
    IF UNICAEN_IMPORT.z__SYNC_FILRE__z IS NULL THEN
      BEGIN
        SELECT sync_filtre INTO sync_filtre FROM import_tables WHERE table_name = '$tableName';
      EXCEPTION WHEN NO_DATA_FOUND THEN
        sync_filtre := '';
      END;
    END IF;
  
    sql_query := 'SELECT V_DIFF_$tableName.* FROM V_DIFF_$tableName ' || COALESCE(UNICAEN_IMPORT.z__SYNC_FILRE__z,sync_filtre);
    OPEN diff_cur FOR sql_query;
    LOOP
      FETCH diff_cur INTO diff_row; EXIT WHEN diff_cur%NOTFOUND;
      BEGIN

        CASE diff_row.import_action
          WHEN 'insert' THEN
            INSERT INTO $tableName
              ( id, " . $this->formatColQuery($cols) . ", source_code, histo_createur_id, histo_modificateur_id )
            VALUES
              ( COALESCE(diff_row.id,$tableName" . "_ID_SEQ.NEXTVAL), " . $this->formatColQuery($cols, 'diff_row.:column') . ", diff_row.source_code, unicaen_import.get_current_user, unicaen_import.get_current_user );

          WHEN 'update' THEN
            " . $this->formatColQuery(
                $cols,
                "IF (diff_row.u_:column = 1 AND IN_COLUMN_LIST(':column',UNICAEN_IMPORT.z__IGNORE_UPD_COLS__z) = 0) THEN UPDATE $tableName SET :column = diff_row.:column WHERE ID = diff_row.id; END IF;"
                , "\n          "
            ) . "

          WHEN 'delete' THEN
            UPDATE $tableName SET histo_destruction = SYSDATE, histo_destructeur_id = unicaen_import.get_current_user WHERE ID = diff_row.id;

          WHEN 'undelete' THEN
            " . $this->formatColQuery(
                $cols,
                "IF (diff_row.u_:column = 1 AND IN_COLUMN_LIST(':column',UNICAEN_IMPORT.z__IGNORE_UPD_COLS__z) = 0) THEN UPDATE $tableName SET :column = diff_row.:column WHERE ID = diff_row.id; END IF;"
                , "\n          "
            ) . "
            UPDATE $tableName SET histo_destruction = NULL, histo_destructeur_id = NULL WHERE ID = diff_row.id;

        END CASE;

      EXCEPTION WHEN OTHERS THEN
        " . $this->getPackage() . ".SYNC_LOG( SQLERRM, '$tableName', diff_row.source_code );
      END;
    END LOOP;
    CLOSE diff_cur;

  END $tableName;";

        return $sql;
    }



    /**
     * Retourne une chaîne SQL correspondant, pour chaque colonne donnée, au résultat du formatage donné,
     * concaténé selon le séparateur transmis.
     *
     * L'opérateur $c permet de situer l'endroit où devont être placées les colonnes.
     *
     * @param array  $cols
     * @param string $format
     * @param string $separator
     *
     * @return string
     */
    protected function formatColQuery(array $cols, $format = ':column', $separator = ',')
    {
        $res = [];
        foreach ($cols as $col) {
            if (is_callable($format)) {
                $f = $format($col);
            } else {
                $f = $format;
            }

            $res[] = str_replace(':column', $col, $f);
        }

        return implode($separator, $res);
    }
}