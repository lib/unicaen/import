<?php

namespace UnicaenImport\Service;

use Doctrine\ORM\EntityManager;
use UnicaenApp\Service\EntityManagerAwareInterface;
use UnicaenApp\Service\EntityManagerAwareTrait;
use UnicaenImport\Exception\Exception;
use ZfcUser\Entity\UserInterface;
use UnicaenAuthentification\Service\DbUserAwareInterface;
use Application\Entity\Db\Utilisateur;

/**
 * Classe mère des services
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class AbstractService implements EntityManagerAwareInterface, DbUserAwareInterface
{
    use EntityManagerAwareTrait;

    /**
     * utilisateur courant
     *
     * @var UserInterface
     */
    protected $currentUser;



    /**
     * Echappe une chaîne de caractères pour convertir en SQL
     *
     * @param string $string
     *
     * @return string
     */
    public static function escapeKW($string)
    {
        return '"' . str_replace('"', '""', strtoupper($string)) . '"';
    }



    /**
     * Echappe une valeur pour convertir en SQL
     *
     * @param mixed $value
     *
     * @return string
     */
    public static function escape($value)
    {
        if (null === $value) return 'NULL';
        switch (gettype($value)) {
            case 'string':
                return "'" . str_replace("'", "''", $value) . "'";
            case 'integer':
                return (string)$value;
            case 'boolean':
                return $value ? '1' : '0';
            case 'double':
                return (string)$value;
            case 'array':
                return '(' . implode(',', array_map(__class__ . '::escape', $value)) . ')';
        }
        throw new Exception('La valeur transmise ne peut pas être convertie en SQL');
    }



    /**
     * Retourne le code SQL correspondant à la valeur transmise, précédé de "=", "IS" ou "IN" suivant le contexte.
     *
     * @param mixed $value
     *
     * @return string
     */
    public static function equals($value)
    {
        if (null === $value) {
            $eq = ' IS ';
        } elseif (is_array($value)) $eq = ' IN ';
        else                        $eq = ' = ';

        return $eq . self::escape($value);
    }



    /**
     * Retourne une tableau des résultats de la requête transmise.
     *
     *
     * @param string $sql
     * @param array  $params
     * @param string $colRes
     *
     * @return array
     */
    protected function query($sql, $params = null, $colRes = null)
    {
        $stmt   = $this->getEntityManager()->getConnection()->executeQuery($sql, $params);
        $result = [];
        while ($r = $stmt->fetch()) {
            if (empty($colRes)) $result[] = $r; else $result[] = $r[$colRes];
        }

        return $result;
    }



    /**
     * exécute un ordre SQL
     *
     * @param string $sql
     *
     * @return integer
     */
    protected function exec($sql)
    {
        return $this->getEntityManager()->getConnection()->exec($sql);
    }



    /**
     *
     * @return UserInterface
     */
    public function getDbUser()
    {
        if (null === $this->currentUser) {
            $this->currentUser = $this->getAppDbUser();
        }

        return $this->currentUser;
    }



    /**
     *  Set Current User
     *
     * @param UserInterface $currentUser
     */
    public function setDbUser(UserInterface $currentUser)
    {
        $this->currentUser = $currentUser;
    }



    /**
     *
     * @return UserInterface
     */
    public function getAppDbUser()
    {
        return $this->getEntityManager()->find(\Application\Entity\Db\Utilisateur::class, Utilisateur::APP_UTILISATEUR_ID);
    }
}