<?php

namespace UnicaenImport\Service\Traits;

use UnicaenImport\Service\SchemaService;
use RuntimeException;

/**
 * Description of SchemaServiceAwareTrait
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
trait SchemaServiceAwareTrait
{
    /**
     * @var SchemaService
     */
    private $serviceSchema;



    /**
     * @param SchemaService $serviceSchema
     *
     * @return self
     */
    public function setServiceSchema(SchemaService $serviceSchema)
    {
        $this->serviceSchema = $serviceSchema;

        return $this;
    }



    /**
     * @return SchemaService
     * @throws RuntimeException
     */
    public function getServiceSchema()
    {
        return $this->serviceSchema;
    }
}