<?php

namespace UnicaenImport\Service\Traits;

use UnicaenImport\Service\DifferentielService;
use RuntimeException;

/**
 * Description of DifferentielServiceAwareTrait
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
trait DifferentielServiceAwareTrait
{
    /**
     * @var DifferentielService
     */
    private $serviceDifferentiel;



    /**
     * @param DifferentielService $serviceDifferentiel
     *
     * @return self
     */
    public function setServiceDifferentiel(DifferentielService $serviceDifferentiel)
    {
        $this->serviceDifferentiel = $serviceDifferentiel;

        return $this;
    }



    /**
     * @return DifferentielService
     * @throws RuntimeException
     */
    public function getServiceDifferentiel()
    {
        return $this->serviceDifferentiel;
    }
}