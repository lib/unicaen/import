<?php

namespace UnicaenImport\Service\Traits;

use UnicaenImport\Service\TableService;

/**
 * Description of TableServiceAwareTrait
 *
 * @author UnicaenCode
 */
trait TableServiceAwareTrait
{
    /**
     * @var TableService
     */
    protected $serviceTable;



    /**
     * @param TableService $serviceTable
     * @return self
     */
    public function setServiceTable( TableService $serviceTable )
    {
        $this->serviceTable = $serviceTable;

        return $this;
    }



    /**
     * @return TableService
     */
    public function getServiceTable() : TableService
    {
        return $this->serviceTable;
    }
}