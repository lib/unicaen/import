<?php

namespace UnicaenImport\Service\Traits;

use UnicaenImport\Service\QueryGeneratorService;
use RuntimeException;

/**
 * Description of QueryGeneratorServiceAwareTrait
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
trait QueryGeneratorServiceAwareTrait
{
    /**
     * @var QueryGeneratorService
     */
    private $serviceQueryGenerator;



    /**
     * @param QueryGeneratorService $serviceQueryGenerator
     *
     * @return self
     */
    public function setServiceQueryGenerator(QueryGeneratorService $serviceQueryGenerator)
    {
        $this->serviceQueryGenerator = $serviceQueryGenerator;

        return $this;
    }



    /**
     * @return QueryGeneratorService
     * @throws RuntimeException
     */
    public function getServiceQueryGenerator()
    {
        return $this->serviceQueryGenerator;
    }
}