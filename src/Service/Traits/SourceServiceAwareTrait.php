<?php

namespace UnicaenImport\Service\Traits;

use UnicaenImport\Service\SourceService;

/**
 * Description of SourceServiceAwareTrait
 *
 * @author UnicaenCode
 */
trait SourceServiceAwareTrait
{
    /**
     * @var SourceService
     */
    protected $serviceSource;



    /**
     * @param SourceService $serviceSource
     * @return self
     */
    public function setServiceSource( SourceService $serviceSource )
    {
        $this->serviceSource = $serviceSource;

        return $this;
    }



    /**
     * @return SourceService
     */
    public function getServiceSource() : SourceService
    {
        return $this->serviceSource;
    }
}