<?php

namespace UnicaenImport\Service;

use UnicaenApp\Service\EntityManagerAwareInterface;
use UnicaenApp\Service\EntityManagerAwareTrait;
use UnicaenImport\Entity\Db\Source;


/**
 * Description of SourceService
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class SourceService implements EntityManagerAwareInterface
{
    use EntityManagerAwareTrait;



    /**
     * @return Source[]
     */
    public function getList()
    {
        return $this->getRepo()->findAll();
    }



    /**
     * @param integer $id
     *
     * @return null|Source
     */
    public function get($id)
    {
        if (!$id) return null;

        return $this->getRepo()->find($id);
    }



    /**
     * @param Source $source
     *
     * @return Source
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Source $source)
    {
        $this->getEntityManager()->persist($source);
        $this->getEntityManager()->flush($source);

        return $source;
    }



    public function delete(Source $source)
    {
        $this->getEntityManager()->remove($source);
        $this->getEntityManager()->flush($source);

        return $this;
    }



    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepo()
    {
        return $this->getEntityManager()->getRepository(Source::class);
    }
}