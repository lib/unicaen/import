<?php

namespace UnicaenImport\Service\Factory;

use UnicaenImport\Options\ModuleOptions;
use UnicaenImport\Service\QueryGeneratorService;
use UnicaenImport\Service\SchemaService;
use UnicaenImport\Service\TableService;
use Psr\Container\ContainerInterface;


/**
 * Description of QueryGeneratorServiceFactory
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class QueryGeneratorServiceFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return QueryGeneratorService
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        $service = new QueryGeneratorService();
        $service->setServiceSchema( $container->get(SchemaService::class));
        $service->setServiceTable( $container->get(TableService::class));
        $service->setOptionsModule($container->get(ModuleOptions::class));

        return $service;
    }
}