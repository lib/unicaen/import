<?php

namespace UnicaenImport\Service\Factory;

use UnicaenImport\Service\DifferentielService;
use UnicaenImport\Service\QueryGeneratorService;
use Psr\Container\ContainerInterface;


/**
 * Description of DifferentielServiceFactory
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class DifferentielServiceFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return DifferentielService
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        $service = new DifferentielService();
        $service->setServiceQueryGenerator( $container->get(QueryGeneratorService::class));

        return $service;
    }
}