<?php

namespace UnicaenImport\Service\Factory;

use UnicaenImport\Service\QueryGeneratorService;
use Psr\Container\ContainerInterface;
use UnicaenImport\Service\TableService;



/**
 * Description of TableServiceFactory
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class TableServiceFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return TableService
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        $service = new TableService;

        return $service;
    }
}