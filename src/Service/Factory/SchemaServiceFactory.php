<?php

namespace UnicaenImport\Service\Factory;

use UnicaenImport\Service\SchemaService;
use Psr\Container\ContainerInterface;


/**
 * Description of SchemaServiceFactory
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class SchemaServiceFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return SchemaService
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        $service = new SchemaService();

        return $service;
    }
}