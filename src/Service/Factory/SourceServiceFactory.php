<?php

namespace UnicaenImport\Service\Factory;

use Psr\Container\ContainerInterface;
use UnicaenImport\Service\SourceService;



/**
 * Description of SourceServiceFactory
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class SourceServiceFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return SourceService
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        $service = new SourceService;
        /* Injectez vos dépendances ICI */

        return $service;
    }
}