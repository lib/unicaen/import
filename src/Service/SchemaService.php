<?php

namespace UnicaenImport\Service;

use UnicaenImport\Exception\Exception;
use UnicaenImport\Entity\Schema\Column;

/**
 *
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
class SchemaService extends AbstractService
{
    /**
     * Schéma
     *
     * @var array
     */
    protected $schema;

    /**
     * @var array
     */
    protected $importColsCache = [];



    /**
     * Retourne le schéma de la BDD
     *
     * @return array
     */
    public function getSchema($tableName = null)
    {
        if (empty($this->schema)) {
            $this->schema = $this->makeSchema();
        }
        if (empty($tableName)) {
            return $this->schema;
        } elseif (array_key_exists($tableName, $this->schema)) {
            return $this->schema[$tableName];
        } else {
            return null;
        }
    }



    /**
     * @return Column[][]
     */
    private function makeSchema()
    {
        $sql = 'SELECT * FROM V_IMPORT_TAB_COLS';
        $d   = $this->query($sql, []);

        $sc = [];
        foreach ($d as $col) {
            $column                                      = new Column;
            $column->table                               = $col['TABLE_NAME'];
            $column->name                                = $col['COLUMN_NAME'];
            $column->isKey                               = $col['IS_KEY'] == '1';
            $column->dataType                            = $col['DATA_TYPE'];
            $column->length                              = (null === $col['LENGTH']) ? null : (integer)$col['LENGTH'];
            $column->nullable                            = $col['NULLABLE'] == '1';
            $column->hasDefault                          = $col['HAS_DEFAULT'] == '1';
            $column->refTableName                        = $col['C_TABLE_NAME'];
            $column->refColumnName                       = $col['C_COLUMN_NAME'];
            $column->importActif                         = $col['IMPORT_ACTIF'] == '1';
            $sc[$col['TABLE_NAME']][$col['COLUMN_NAME']] = $column;
        }

        return $sc;
    }



    /**
     * retourne la liste des tables supportées par l'import automatique
     *
     * @return array
     */
    public function getImportTables()
    {
        $sql = "SELECT 
          it.table_name 
        FROM 
          import_tables it
          JOIN user_views v ON v.view_name = 'SRC_' || it.table_name
        WHERE 
          it.sync_enabled = 1 
        ORDER BY 
          it.ordre";

        return $this->query($sql, [], 'TABLE_NAME');
    }



    public function getNonReferencedImportTables()
    {
        $sql = "
        SELECT DISTINCT
          ut.table_name
        FROM
          user_tab_cols utc
          JOIN user_tables ut ON ut.table_name = utc.table_name
          LEFT JOIN import_tables it ON it.table_name = ut.table_name
          LEFT JOIN user_mviews um ON um.mview_name = ut.table_name
        WHERE
          (utc.column_name = 'SOURCE_ID' OR utc.column_name = 'SOURCE_CODE')
          AND utc.table_name <> 'SYNC_LOG'
          AND it.table_name IS NULL
          AND um.mview_name IS NULL
        ORDER BY
          ut.table_name
        ";

        return $this->query($sql, [], 'TABLE_NAME');
    }



    /**
     * Retourne la liste des tables ayant des vues matérialisées
     *
     * @return string[]
     */
    public function getImportMviews()
    {
        $sql    = "SELECT mview_name FROM USER_MVIEWS WHERE mview_name LIKE 'MV_%'";
        $stmt   = $this->getEntityManager()->getConnection()->query($sql);
        $mviews = [];
        while ($d = $stmt->fetch()) {
            $mvn      = substr($d['MVIEW_NAME'], 3);
            $mviews[] = $mvn;
        }

        return $mviews;
    }



    /**
     * Retourne la liste des tables ayant des vues matérialisées
     *
     * @return string[]
     */
    public function getImportViews(): array
    {
        $sql   = "SELECT view_name FROM USER_VIEWS WHERE view_name LIKE 'SRC_%'";
        $stmt  = $this->getEntityManager()->getConnection()->query($sql);
        $views = [];
        while ($d = $stmt->fetch()) {
            $mvn     = substr($d['VIEW_NAME'], 4);
            $views[] = $mvn;
        }

        return $views;
    }



    /**
     *
     * @param string $tableName
     * @param string $columnName
     */
    public function hasColumn(string $tableName, string $columnName): bool
    {
        $sql    = "
        SELECT
          COUNT(*) result
        FROM
          USER_TAB_COLS utc
        WHERE
          utc.table_name = :tableName
          AND utc.column_name = :columnName
        ";
        $result = $this->query($sql, compact('tableName', 'columnName'), 'RESULT');

        return $result[0] === '1';
    }



    /**
     * Retourne les colonnes concernées par l'import pour une table donnée
     */
    public function getImportCols($tableName): array
    {
        if (!array_key_exists($tableName, $this->importColsCache)) {
            $sql = "
            SELECT
                utc.COLUMN_NAME
            FROM
              USER_TAB_COLS utc
              JOIN USER_TAB_COLS atc ON (atc.table_name = 'SRC_' || utc.table_name AND atc.column_name = utc.column_name)
            WHERE
              utc.COLUMN_NAME NOT IN ('ID')
              AND utc.COLUMN_NAME NOT LIKE 'HISTO_%'
              AND utc.COLUMN_NAME <> 'SOURCE_CODE'
              AND utc.table_name = :tableName
            ORDER BY
              utc.COLUMN_NAME";

            $this->importColsCache[$tableName] = $this->query($sql, ['tableName' => $tableName], 'COLUMN_NAME');
        }

        return $this->importColsCache[$tableName];
    }



    /**
     * Détecte si une propriété d'une classe est importable ou non
     *
     * @param object|string $entity
     * @param string $fieldName
     *
     * @return bool
     */
    public function isImportedProperty(object|string $entity, string $fieldName): bool
    {
        if (is_object($entity)){
            $entity = get_class($entity);
        }

        $metadata = $this->getEntityManager()->getClassMetadata($entity);

        $tableName = $metadata->getTableName();

        if ($metadata->hasAssociation($fieldName)) {
            $association = $metadata->getAssociationMapping($fieldName);
            $columnName = isset($association['joinColumns'][0]['name']) ? $association['joinColumns'][0]['name'] : $fieldName;
        } else {
            $columnName = $metadata->getColumnName($fieldName);
        }

        $importCols = $this->getImportCols($tableName);

        return in_array($columnName, $importCols);
    }



    /**
     * @param object|string $entity
     *
     * @return bool
     */
    public function isImportedEntity(object|string $entity): bool
    {
        if (is_object($entity)){
            $entity = get_class($entity);
        }

        $metadata = $this->getEntityManager()->getClassMetadata($entity);

        $tableName = $metadata->getTableName();

        return in_array($tableName, $this->getImportTables());
    }
}