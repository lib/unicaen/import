<?php

namespace UnicaenImport\Form\Factory;

use Psr\Container\ContainerInterface;
use UnicaenImport\Form\TableForm;

/**
 * Description of TableFormFactory
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class TableFormFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        return new TableForm();
    }
}