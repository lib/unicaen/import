<?php

namespace UnicaenImport\Form\Factory;

use Psr\Container\ContainerInterface;
use UnicaenImport\Form\SourceForm;

/**
 * Description of SourceFormFactory
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class SourceFormFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        return new SourceForm;
    }
}