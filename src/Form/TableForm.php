<?php

namespace UnicaenImport\Form;

use UnicaenImport\Entity\Db\Table;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Hydrator\HydratorInterface;


/**
 * Description of TableForm
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class TableForm extends Form implements InputFilterProviderInterface
{

    public function init()
    {
        $hydrator = new TableFormHydrator;
        $this->setHydrator($hydrator);
        $this->setAttribute('class', 'tables-edit');

        $this->add([
            'name'    => 'syncEnabled',
            'options' => [
                'label' => 'Synchro activée',
            ],
            'type'    => 'Checkbox',
        ]);

        $this->add([
            'name'    => 'syncJob',
            'options' => [
                'label' => 'Job de synchronisation',
            ],
            'type'    => 'Text',
        ]);

        $this->add([
            'name'    => 'syncFiltre',
            'options' => [
                'label' => 'Filtre de synchronisation (SQL partiel) <div class="alert alert-warning" style="padding:2px;margin-bottom:1px;font-size:8pt">Attention : Si vous utilisez un filtre "OR", pensez bien à entourer l\'ensemble de votre expression avec des parenthèses, sans quoi le filtre provoquera des dysfonctionnements</div>',
                'label_options' => ['disable_html_escape' => true],
            ],
            'attributes' => [
                'id' => 'sync_filtre',
                'rows'  => '5',
            ],
            'type'    => 'Textarea',
        ]);

        $this->add([
            'name'    => 'syncHookBefore',
            'options' => [
                'label' => 'À exécuter avant la synchro (code PL/SQL)',
            ],
            'attributes' => [
                'id' => 'sync_hook_before',
                'rows'  => '5',
            ],
            'type'    => 'Textarea',
        ]);

        $this->add([
            'name'    => 'syncHookAfter',
            'options' => [
                'label' => 'À exécuter après la synchro (code PL/SQL)',
            ],
            'attributes' => [
                'id' => 'sync_hook_after',
                'rows'  => '5',
            ],
            'type'    => 'Textarea',
        ]);

        $this->add([
            'name'       => 'submit',
            'type'       => 'Submit',
            'attributes' => [
                'value' => 'Enregistrer',
                'class' => 'btn btn-primary btn-save',
            ],
        ]);
    }



    /**
     * Should return an array specification compatible with
     * {@link Laminas\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            'syncEnabled'    => ['required' => true,],
            'syncJob'        => ['required' => false,],
            'syncFiltre'     => ['required' => false,],
            'syncHookBefore' => ['required' => false,],
            'syncHookAfter'  => ['required' => false,],
        ];
    }

}





class TableFormHydrator implements HydratorInterface
{

    /**
     * @param  array $data
     * @param Table  $object
     *
     * @return object
     */
    public function hydrate(array $data, $object)
    {
        $object->setSyncEnabled($data['syncEnabled'] == '1');
        $object->setSyncJob($data['syncJob']);
        $object->setSyncFiltre($data['syncFiltre']);
        $object->setSyncHookBefore($data['syncHookBefore']);
        $object->setSyncHookAfter($data['syncHookAfter']);

        return $object;
    }



    /**
     * @param Source $object
     *
     * @return array
     */
    public function extract($object): array
    {
        $data = [
            'name'           => $object->getName(),
            'syncEnabled'    => $object->isSyncEnabled() ? '1' : '0',
            'syncJob'        => $object->getSyncJob(),
            'syncFiltre'     => $object->getSyncFiltre(),
            'syncHookBefore' => $object->getSyncHookBefore(),
            'syncHookAfter'  => $object->getSyncHookAfter(),
        ];

        return $data;
    }
}