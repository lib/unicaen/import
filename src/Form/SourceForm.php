<?php

namespace UnicaenImport\Form;

use UnicaenImport\Entity\Db\Source;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Hydrator\HydratorInterface;


/**
 * Description of SourceForm
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class SourceForm extends Form implements InputFilterProviderInterface
{

    public function init()
    {
        $hydrator = new SourceFormHydrator;
        $this->setHydrator($hydrator);

        $this->add([
            'name' => 'id',
            'type' => 'hidden',
        ]);

        $this->add([
            'name'    => 'code',
            'options' => [
                'label' => 'Code',
            ],
            'type'    => 'Text',
        ]);

        $this->add([
            'name'    => 'libelle',
            'options' => [
                'label' => 'Libellé',
            ],
            'type'    => 'Text',
        ]);

        $this->add([
            'name'    => 'importable',
            'options' => [
                'label' => 'Importable',
            ],
            'type'    => 'Checkbox',
        ]);

        $this->add([
            'name'       => 'submit',
            'type'       => 'Submit',
            'attributes' => [
                'value' => 'Enregistrer',
                'class' => 'btn btn-primary',
            ],
        ]);
    }



    /**
     * Should return an array specification compatible with
     * {@link Laminas\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            /* Filtres et validateurs */
        ];
    }

}





class SourceFormHydrator implements HydratorInterface
{

    /**
     * @param  array $data
     * @param Source $object
     *
     * @return object
     */
    public function hydrate(array $data, $object)
    {
        $object->setCode($data['code']);
        $object->setLibelle($data['libelle']);
        $object->setImportable($data['importable'] == '1');

        return $object;
    }



    /**
     * @param Source $object
     *
     * @return array
     */
    public function extract($object): array
    {
        $data = [
            'id'         => $object->getId(),
            'code'       => $object->getCode(),
            'libelle'    => $object->getLibelle(),
            'importable' => $object->getImportable() ? '1' : '0',
        ];

        return $data;
    }
}