<?php

namespace UnicaenImport\Form\Traits;

use UnicaenImport\Form\TableForm;

/**
 * Description of TableFormAwareTrait
 *
 * @author UnicaenCode
 */
trait TableFormAwareTrait
{
    /**
     * @var TableForm
     */
    protected $formTable;



    /**
     * @param TableForm $formTable
     *
     * @return self
     */
    public function setFormTable(TableForm $formTable)
    {
        $this->formTable = $formTable;

        return $this;
    }



    /**
     * Retourne un nouveau formulaire ou fieldset systématiquement, sauf si ce dernier a été fourni manuellement.
     *
     * @return TableForm
     * @throws RuntimeException
     */
    public function getFormTable(): TableForm
    {
        return $this->formTable;
    }
}