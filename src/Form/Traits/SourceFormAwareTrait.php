<?php

namespace UnicaenImport\Form\Traits;

use UnicaenImport\Form\SourceForm;

/**
 * Description of SourceFormAwareTrait
 *
 * @author UnicaenCode
 */
trait SourceFormAwareTrait
{
    /**
     * @var SourceForm
     */
    protected $formSource;



    /**
     * @param SourceForm $formSource
     *
     * @return self
     */
    public function setFormSource(SourceForm $formSource)
    {
        $this->formSource = $formSource;

        return $this;
    }



    /**
     * Retourne un nouveau formulaire ou fieldset systématiquement, sauf si ce dernier a été fourni manuellement.
     *
     * @return SourceForm
     * @throws RuntimeException
     */
    public function getFormSource(): SourceForm
    {
        return $this->formSource;
    }
}