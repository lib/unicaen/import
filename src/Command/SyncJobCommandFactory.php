<?php

namespace UnicaenImport\Command;

use Psr\Container\ContainerInterface;
use UnicaenImport\Processus\ImportProcessus;


/**
 * Description of SyncJobCommandFactory
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class SyncJobCommandFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return SyncJobCommand
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null): SyncJobCommand
    {
        $command = new SyncJobCommand;

        $command->setProcessusImport($container->get(ImportProcessus::class));

        return $command;
    }
}