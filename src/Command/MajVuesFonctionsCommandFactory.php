<?php

namespace UnicaenImport\Command;

use Psr\Container\ContainerInterface;
use UnicaenImport\Processus\ImportProcessus;


/**
 * Description of MajVuesFonctionsCommandFactory
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class MajVuesFonctionsCommandFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return MajVuesFonctionsCommand
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null): MajVuesFonctionsCommand
    {
        $command = new MajVuesFonctionsCommand;

        $command->setProcessusImport($container->get(ImportProcessus::class));

        return $command;
    }
}