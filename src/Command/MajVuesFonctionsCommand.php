<?php

namespace UnicaenImport\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use UnicaenImport\Processus\Traits\ImportProcessusAwareTrait;

/**
 * Description of MajVuesFonctionsCommand
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class MajVuesFonctionsCommand extends Command
{
    use ImportProcessusAwareTrait;

    protected function configure(): void
    {
        $this->setDescription('Mise à jour des vues différentielles & des procédures de MAJ');
    }



    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io  = new SymfonyStyle($input, $output);

        $io->title($this->getDescription());

        try {
            $this->getProcessusImport()->updateViewsAndPackages();
            $io->success('Mise à jour des vues différentielles et du paquetage d\'import terminés');
        } catch (\Exception $e) {
            echo 'Une erreur a été rencontrée.';
        }

        return Command::SUCCESS;
    }
}