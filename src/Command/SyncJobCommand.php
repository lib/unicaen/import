<?php

namespace UnicaenImport\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use UnicaenImport\Processus\Traits\ImportProcessusAwareTrait;

/**
 * Description of SyncJobCommand
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class SyncJobCommand extends Command
{
    use ImportProcessusAwareTrait;

    protected function configure(): void
    {
        $this
            ->setDescription('Synchronisation en import')
            ->addArgument('job', InputArgument::REQUIRED, 'Nom du job que vous souhaitez lancer');
        ;
    }



    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io  = new SymfonyStyle($input, $output);

        $io->title($this->getDescription());

        $job = $input->getArgument('job');
        if (empty($job)) {
            $io->error("Vous devez préciser le job à lancer");
            return Command::FAILURE;

        }

        $this->getProcessusImport()->syncJob($job);

        return Command::SUCCESS;
    }
}