<?php

namespace UnicaenImport\View\Helper\Factory;

use UnicaenImport\View\Helper\DifferentielListe;
use Psr\Container\ContainerInterface;


/**
 * Description of DifferentielListeViewHelperFactory
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class DifferentielListeViewHelperFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return DifferentielLigne
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        $viewHelper = new DifferentielListe();

        return $viewHelper;
    }
}