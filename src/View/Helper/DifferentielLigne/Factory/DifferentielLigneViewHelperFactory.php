<?php

namespace UnicaenImport\View\Helper\DifferentielLigne\Factory;

use UnicaenImport\Options\ModuleOptions;
use UnicaenImport\View\Helper\DifferentielLigne\DifferentielLigne;
use Psr\Container\ContainerInterface;


/**
 * Description of DifferentielLigneViewHelperFactory
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class DifferentielLigneViewHelperFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return DifferentielLigne
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        $viewHelper = new DifferentielLigne();
        $viewHelper->setOptionsModule($container->get(ModuleOptions::class));

        return $viewHelper;
    }
}