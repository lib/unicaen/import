<?php

namespace UnicaenImport\Exception;

/**
 *
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class MissingDependency extends Exception
{

}