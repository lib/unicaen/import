<?php

namespace UnicaenImport\Options\Factory;

use UnicaenImport\Options\ModuleOptions;
use Psr\Container\ContainerInterface;


/**
 * Description of ModuleOptionsFactory
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class ModuleOptionsFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return ModuleOptions
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        $config = $container->get('Config');
        $configUnicaenImport = isset($config['unicaen-import']) ? $config['unicaen-import'] : [];

        $service = new ModuleOptions($configUnicaenImport);

        return $service;
    }
}