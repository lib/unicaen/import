<?php

namespace UnicaenImport\Options\Traits;

use UnicaenImport\Options\ModuleOptions;
use RuntimeException;

/**
 * Description of ModuleOptionsAwareTrait
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
trait ModuleOptionsAwareTrait
{
    /**
     * @var ModuleOptions
     */
    private $optionsModule;



    /**
     * @param ModuleOptions $optionsModule
     *
     * @return self
     */
    public function setOptionsModule(ModuleOptions $optionsModule)
    {
        $this->optionsModule = $optionsModule;

        return $this;
    }



    /**
     * @return ModuleOptions
     * @throws RuntimeException
     */
    public function getOptionsModule()
    {
        return $this->optionsModule;
    }
}