<?php

namespace UnicaenImport\Options;

use Laminas\Stdlib\AbstractOptions;
use Laminas\Stdlib\ArrayUtils;

class ModuleOptions extends AbstractOptions
{
    /**
     * Turn off strict options mode
     */
    protected $__strictMode__ = false;

    /**
     * @var string
     */
    protected $package = 'UNICAEN_IMPORT';

    /**
     * @var array
     */
    protected $differentielViewHelpers = [];

    /**
     * @var array
     */
    protected $entitySourceInjector;

    /**
     * @var string 
     */
    protected $jsAceUrl = '/vendor/unicaen/import/ace-builds-master/src-min/ace.js';



    /**
     * @return string
     */
    public function getPackage()
    {
        return $this->package;
    }



    /**
     * @param string $package
     *
     * @return ModuleOptions
     */
    public function setPackage($package)
    {
        $this->package = $package;

        return $this;
    }



    /**
     * @return array
     */
    public function getDifferentielViewHelpers()
    {
        return $this->differentielViewHelpers;
    }



    /**
     * @param array $differentielViewHelpers
     *
     * @return ModuleOptions
     */
    public function setDifferentielViewHelpers($differentielViewHelpers)
    {
        $this->differentielViewHelpers = $differentielViewHelpers;

        return $this;
    }



    /**
     * @return string
     */
    public function getEntitySourceInjector()
    {
        return $this->entitySourceInjector;
    }



    /**
     * @param string $entitySourceInjector
     *
     * @return ModuleOptions
     */
    public function setEntitySourceInjector($entitySourceInjector)
    {
        $this->entitySourceInjector = $entitySourceInjector;

        return $this;
    }



    /**
     * @return string
     */
    public function getJsAceUrl(): string
    {
        return $this->jsAceUrl;
    }



    /**
     * @param string $jsAceUrl
     *
     * @return ModuleOptions
     */
    public function setJsAceUrl(string $jsAceUrl): ModuleOptions
    {
        $this->jsAceUrl = $jsAceUrl;

        return $this;
    }
    
}