<?php

namespace UnicaenImport;

use Laminas\ModuleManager\Feature\ConfigProviderInterface;
use Laminas\Mvc\MvcEvent;

/**
 *
 *
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */
class Module implements ConfigProviderInterface
{
    public function onBootstrap(MvcEvent $e)
    {

    }



    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }



    public function getConsoleUsage($console)
    {
        return [
            "Exécution de script",
            'UnicaenImport SyncJob <job>' => "Lance un job de synchronisation",
            'UnicaenImport MajVuesFonctions' => "Recrée les vues différnetielles et les procédures de mise à jour",
        ];
    }
}
